# Brillo Developer Kit Schemas

The Brillo Developer Kit provides a number of configuration files for
use by the end developer.  Those files are expressed in XML and the
schema files in this directory provide validation, in RELAX NG, for
structural correctness.

Additional application layer validation is applied, but the schemas
make it easier to know when a given file is semantically valid before
exploring other problems.

The current supported schemas are as follows:

  * [bdk.rng](/schema/bdk.rng): Specifies how to assemble device and developer
    software for defined Brillo devices from packs and targets.
  * [packs.rng](/schema/packs.rng): Specifies a collection of resources and how
    they map into a device.
  * [targets.rng](/schema/targets.rng): Specifies a target device and what tree
    of packs make up its software.

# Format

The schema files are written in [RELAX NG](http://relaxng.org/).

# Testing

When updating the schemas with "breaking" changes, a new version should be
added to the version list for all root nodes (bdk-config, packs, targets) and
the prior version frozen and archived under "versions/<version_number>".

## Quick Tests

Quick validation can be performed with the xmllint tool:

```sh
$ xmllint file.xml - -relaxng bdk.rng
```

Or with better output:

```sh
$ jing bdk.rng file.xml
```

Or in python with:

```python
from lxml import etree
rng_doc = etree.parse('bdk.rng')
rng = etree.RelaxNG(rng_doc)
doc = etree.parse('file.xml')
print rng.validate(doc) # => False
print rng.error_log # => Errors
```
