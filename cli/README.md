# The Brillo Developer Kit (BDK) command line interface (CLI)

This holds all the code (written in Python) for the `bdk` tool: a CLI tool to
manage Brillo product development.
