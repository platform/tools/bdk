#
# Copyright (C) 2016 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

"""Unittests for exception.py."""


import error
from metrics import metrics_util_stub
from metrics.data_types import exception
from metrics.data_types import hit_unittest


class ExceptionTest(hit_unittest.HitTest):
    """Exception unittest class."""

    def setup_hit(self):
        """Set up the fields for testing.

        Attributes:
            hit: The hit object under test.
            expected_fields: The expected fields sent by the hit.
        """
        self.setup_default_hit_fields()

        self.hit = exception.ExceptionHit(
            self.data,
            'exception_description',
            is_fatal=True,
            custom_dimensions=self.custom_dimensions,
            custom_metrics=self.custom_metrics)

        self.expected_fields.update({
            exception.ExceptionHit.GA_KEY_HIT_TYPE: 'exception',
            exception.ExceptionHit.GA_KEY_EXCEPTION_DESCRIPTION:
                'exception_description',
            exception.ExceptionHit.GA_KEY_EXCEPTION_FATAL: True
        })

    def test_from_exception(self):
        """Test exception.ExceptionHit.from_exception() method."""
        metrics_util_stub_ = metrics_util_stub.StubMetricsUtil(self.data, {})

        exception.metrics_util = metrics_util_stub_

        ex = error.Error('Test error')
        self.hit = exception.ExceptionHit.from_exception(ex, is_fatal=True)
        self.expected_fields = {
            exception.ExceptionHit.GA_KEY_PROTOCOL_VERSION: self.data.version,
            exception.ExceptionHit.GA_KEY_TRACKING_ID: self.data.app_id,
            exception.ExceptionHit.GA_KEY_APPLICATION_NAME: self.data.app_name,
            exception.ExceptionHit.GA_KEY_APPLICATION_VERSION:
                self.data.app_version,
            exception.ExceptionHit.GA_KEY_CLIENT_ID: self.data.user_id,
            exception.ExceptionHit.GA_KEY_HIT_TYPE: 'exception',
            exception.ExceptionHit.GA_KEY_EXCEPTION_DESCRIPTION: 'Error',
            exception.ExceptionHit.GA_KEY_EXCEPTION_FATAL: True}
        self.test_get_fields()
        self.test_send_fields()
