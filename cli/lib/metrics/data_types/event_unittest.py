#
# Copyright (C) 2016 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

"""Unittests for event.py."""


from metrics.data_types import event
from metrics.data_types import hit_unittest


class EventTest(hit_unittest.HitTest):
    """Event unittest class."""

    def setup_hit(self):
        """Set up the fields for testing.

        Attributes:
          hit: The hit object under test.
          expected_fields: The expected fields sent by the hit.
        """
        self.setup_default_hit_fields()

        self.hit = event.Event(self.data,
                               'my_category',
                               'my_action',
                               value=200,
                               label='my_label',
                               custom_dimensions=self.custom_dimensions,
                               custom_metrics=self.custom_metrics)

        self.expected_fields.update({
            event.Event.GA_KEY_HIT_TYPE: 'event',
            event.Event.GA_KEY_EVENT_CATEGORY: 'my_category',
            event.Event.GA_KEY_EVENT_ACTION: 'my_action',
            event.Event.GA_KEY_EVENT_LABEL: 'my_label',
            event.Event.GA_KEY_EVENT_VALUE: 200
        })
