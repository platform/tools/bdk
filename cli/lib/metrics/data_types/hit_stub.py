#
# Copyright (C) 2016 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

"""metrics.data_types.hit module stubs."""


from metrics.data_types import hit


class StubHit(object):
    """Stubs the metrics.data_types.hit class.

    Attributes:
        last_send: The last sent fields.
    """

    last_send = None

    def __init__(self):
        StubHit.last_send = None

    @classmethod
    def get_custom_data_field(cls, field):
        if cls.last_send == None:
            return None
        return cls.last_send['cd' + str(field)]

    class Hit(hit.Hit):
        """Stub class that extends a hit object.

        Override the send_fields function to not actually send data, and instead
        store the fields to the StubHit.last_send variable so that it can be
        checked later.
        """

        @classmethod
        def send_fields(cls, fields):
            StubHit.last_send = fields
            return True
