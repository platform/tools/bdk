#
# Copyright (C) 2015 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

"""The base Hit class for Google Analytics hits.

This file defines a base class for GA data, including a constructor that ensures
the required fields are included. Subclasses for each hit type should define
additional fields as necessary.
"""


import urllib

from core import popen


class Hit(object):
    """An abstract class for Analytics hit details.

    Hit serves as a parent class for all types of hits.
    Its fields encompass the values required for all hit types,
    as well as some optional values for all hit types.

    Attributes:
        version: the Analytics Measurement Protocol version being used.
        app_id: the ID of the GA property to send data to.
        app_name: the name of the application.
        user_id: an ID unique to a particular user.
        hit_type: the type of interaction this data represents.
        cds: (optional) a dictionary of { index : value } for custom dimensions.
        cms: (optional) a dictionary of { index : numeric_value } for
             custom metrics.
    """

    GA_EVENT_TYPE = 'event'
    GA_EXCEPTION_TYPE = 'exception'
    GA_TIMING_TYPE = 'timing'

    _GA_ENDPOINT = 'https://ssl.google-analytics.com/collect'

    # GA field keys.
    GA_KEY_PROTOCOL_VERSION = 'v'
    GA_KEY_TRACKING_ID = 'tid'
    GA_KEY_APPLICATION_NAME = 'an'
    GA_KEY_APPLICATION_VERSION = 'av'
    GA_KEY_CLIENT_ID = 'cid'
    GA_KEY_HIT_TYPE = 't'
    GA_KEY_CUSTOM_DIMENSION = 'cd'
    GA_KEY_CUSTOM_METRIC = 'cm'

    def __init__(self, meta_data, hit_type,
                 custom_dimensions=None, custom_metrics=None):
        self.version = meta_data.version
        self.app_id = meta_data.app_id
        self.app_name = meta_data.app_name
        self.app_version = meta_data.app_version
        self.user_id = meta_data.user_id
        self.hit_type = hit_type
        self.cds = custom_dimensions or {}
        self.cms = custom_metrics or {}

    def get_fields(self):
        """Returns all fields as a dictionary of { param : value }."""
        params = {self.GA_KEY_PROTOCOL_VERSION: self.version,
                  self.GA_KEY_TRACKING_ID: self.app_id,
                  self.GA_KEY_APPLICATION_NAME: self.app_name,
                  self.GA_KEY_APPLICATION_VERSION: self.app_version,
                  self.GA_KEY_CLIENT_ID: self.user_id,
                  self.GA_KEY_HIT_TYPE: self.hit_type}
        for i in self.cds:
            params[self.GA_KEY_CUSTOM_DIMENSION + str(i)] = self.cds[i]
        for i in self.cms:
            params[self.GA_KEY_CUSTOM_METRIC + str(i)] = self.cms[i]
        return params

    @classmethod
    def send_fields(cls, fields):
        """Sends the hit described by fields to Google Analytics.

        Args:
            fields - a dictionary of data to send.

        Returns True if the send succeeds, otherwise False.
        """
        data = urllib.urlencode(fields)
        send_process = popen.PopenPiped(['curl', '--data', data,
                                         cls._GA_ENDPOINT])
        send_process.communicate()
        return send_process.returncode == 0
