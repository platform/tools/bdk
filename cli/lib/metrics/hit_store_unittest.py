#
# Copyright (C) 2016 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

"""Unittests for hit_store.py."""


import os
import shutil
import stat
import tempfile
import unittest

from metrics import hit_store


class HitStoreTest(unittest.TestCase):
    """HitStore unittest class."""

    def setUp(self):
        """Set up the test."""
        self.hit_store = hit_store.Backup(file_path=':memory:')

    def test_retrieve_fields(self):
        """Test saving and retrieving fields from a hit store."""
        field1 = {'key1': 'value_a_1', 'key2': 'value_a_2', 'key3': 'value_a_3'}
        field2 = {'key1': 'value_b_1', 'key2': 'value_b_2', 'key3': 'value_b_3'}
        field3 = {'key1': 'value_c_1', 'key2': 'value_c_2', 'key3': 'value_c_3'}
        self.hit_store.save(field1)
        self.hit_store.save(field2)
        retrieved = self.hit_store.retrieve_all()
        self.assertEqual(retrieved, [field1, field2])

        # Verify that the hit store removed any entries already retrieved.
        self.hit_store.save(field3)
        retrieved = self.hit_store.retrieve_all()
        self.assertEqual(retrieved, [field3])


class HitStoreSetupTest(unittest.TestCase):
    """Test that a hit store is set up correctly."""

    def setUp(self):
        """Set up the test."""
        self.tmpdir = tempfile.mkdtemp()
        self.db = os.path.join(self.tmpdir, 'test.db')
        self.hit_store = hit_store.Backup(file_path=self.db)

        # pylint: disable=protected-access
        self.hit_store._setup()

    def tearDown(self):
        """Clean up after the test."""
        shutil.rmtree(self.tmpdir)

    def test_file_permissions(self):
        """Test that the database file has the correct permissions."""
        self.assertTrue(os.path.isfile(self.db))
        stat_ = os.stat(self.db)
        permissions = oct(stat_.st_mode & 0o777)
        self.assertEqual(permissions, oct(stat.S_IRUSR | stat.S_IWUSR))
