#
# Copyright (C) 2015 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

"""Tests for tool.py"""


import unittest

from core import config
from core import tool
from core import util_stub
from project import platform_stub
from test import stubs


class ToolWrapperTest(unittest.TestCase):
    """Tests for the ToolWrapper classes."""

    def setUp(self):
        self.stub_os = stubs.StubOs()
        self.stub_subprocess = stubs.StubSubprocess()
        self.stub_util = util_stub.StubUtil(arch='host_test_arch')

        tool.os = self.stub_os
        tool.util = self.stub_util
        tool.subprocess = self.stub_subprocess

        self.platform = platform_stub.StubPlatform(
            os_version='12.34', os_root='/source', board='board_name',
            should_link=True, device_arch='test_arch',
            build_cache='/build/out',
            product_out_cache='/build/out/target/product/board_name')

    def test_run(self):
        """Tests a basic tool run."""
        t = tool.ToolWrapper('/my/tool')
        command = self.stub_subprocess.AddCommand(['/my/tool'])

        t.run()
        command.AssertCallContained(['/my/tool'])

    def test_run_piped(self):
        """Tests a basic piped tool run."""
        t = tool.ToolWrapper('/my/tool')
        self.stub_subprocess.AddCommand(['/my/tool'], ret_out='out',
                                        ret_err='err')
        self.stub_subprocess.AddCommand(['/my/tool'], ret_out='out',
                                        ret_err='err')
        self.assertEqual(t.run(), (None, None))
        self.assertEqual(t.run(piped=True), ('out', 'err'))


    def test_cwd(self):
        """Tests changing the tool working directory."""
        t = tool.ToolWrapper('/my/tool')
        t.set_cwd('/here')
        command = self.stub_subprocess.AddCommand(['/my/tool'])

        t.run()
        command.AssertCallContained(cwd='/here', env={'PWD': '/here'})

    def test_fail(self):
        """Tests a failed execution call."""
        def fail_subprocess_call():
            error = OSError()
            error.strerror = 'blah'
            raise error

        t = tool.ToolWrapper('/does/not/exist')
        self.stub_subprocess.AddCommand(side_effect=fail_subprocess_call)

        with self.assertRaises(tool.ExecuteError) as error:
            t.run()
        self.assertIn('/does/not/exist', str(error.exception))
        self.assertIn('blah', str(error.exception))

    def test_custom_env(self):
        """Tests passing custom environment variables."""
        t = tool.ToolWrapper('/my/tool', env={'foo': '1'})
        t.environment['bar'] = '2'
        command = self.stub_subprocess.AddCommand(['/my/tool'])

        t.run()
        command.AssertCallContained(env={'foo': '1', 'bar': '2'})

    def test_inherited_env(self):
        """Tests inheriting environment variables."""
        self.stub_os.environ['USER'] = 'me'
        self.stub_os.environ['PATH'] = '/should/not/inherit'
        t = tool.ToolWrapper('/my/tool')
        command = self.stub_subprocess.AddCommand(['/my/tool'])

        t.run()
        command.AssertCallContained(env={'USER': 'me'})

    def test_add_caller_env_path(self):
        """Tests adding the caller's environment PATH manually."""
        # No tool or caller PATH.
        t = tool.ToolWrapper('/my/tool')
        t.add_caller_env_path()
        self.assertNotIn('PATH', t.environment)

        # Tool PATH only.
        t = tool.ToolWrapper('/my/tool', env={'PATH': '/foo'})
        t.add_caller_env_path()
        self.assertEqual('/foo', t.environment['PATH'])

        # Caller PATH only.
        self.stub_os.environ['PATH'] = '/bar'
        t = tool.ToolWrapper('/my/tool')
        t.add_caller_env_path()
        self.assertEqual('/bar', t.environment['PATH'])

        # Both PATHs set.
        self.stub_os.environ['PATH'] = '/bar'
        t = tool.ToolWrapper('/my/tool', env={'PATH': '/foo'})
        t.add_caller_env_path()
        self.assertEqual('/foo:/bar', t.environment['PATH'])

    def test_android_env(self):
        """Tests setting android environment."""
        t = tool.ToolWrapper('/my/tool')

        t.set_android_environment(self.platform)
        command = self.stub_subprocess.AddCommand(['/my/tool'])

        t.run()
        command.AssertCallContained(env={
            'ANDROID_BUILD_TOP': '/source',
            'ANDROID_HOST_OUT': '/build/out/host/host_test_arch',
            'ANDROID_PRODUCT_OUT': '/build/out/target/product/board_name'
        })

    def test_host_tool_wrapper(self):
        """Tests HostToolWrapper construction."""
        self.stub_util.arch = 'test_arch'
        t = tool.HostToolWrapper('tool', self.platform)
        self.stub_subprocess.AddCommand(['/build/out/host/test_arch/bin/tool'])

        t.run()

    def test_path_tool_wrapper(self):
        self.stub_os.environ = {'foo': 'bar', 'baz': 'bip', 'PATH': '/usr/bin'}
        t = tool.PathToolWrapper('tool')
        command = self.stub_subprocess.AddCommand(['tool'])
        t.run()
        command.AssertCallContained(['tool'], env={'PATH': '/usr/bin'})

    def test_provision_device_tool(self):
        """Tests ProvisionDeviceTool construction."""
        self.stub_util.arch = 'test_arch'
        self.stub_util.os_version = '12.34'
        t = tool.ProvisionDeviceTool(self.platform, '/alternate/product')
        command = self.stub_subprocess.AddCommand()

        t.run()
        command.AssertCallContained(
            ['/alternate/product/provision-device'],
            env={'ANDROID_BUILD_TOP': '/source',
                 'ANDROID_HOST_OUT': '/build/out/host/test_arch',
                 'ANDROID_PRODUCT_OUT': '/alternate/product'})


class BrunchToolWrapperTest(unittest.TestCase):

    def setUp(self):
        self.stub_subprocess = stubs.StubSubprocess()
        self.stub_util = util_stub.StubUtil()

        tool.util = self.stub_util
        tool.os = stubs.StubOs()
        tool.subprocess = self.stub_subprocess

        self.saved_props = (config.DictStore.REQUIRED_PROPS,
                            config.DictStore.OPTIONAL_PROPS)
        config.DictStore.REQUIRED_PROPS = config.ProductFileStore.REQUIRED_PROPS
        config.DictStore.OPTIONAL_PROPS = config.ProductFileStore.OPTIONAL_PROPS
        self.store = config.DictStore()
        self.store.device = 'somedevice'

        self.product_path = 'product_path'

    def tearDown(self):
        config.DictStore.REQUIRED_PROPS = self.saved_props[0]
        config.DictStore.OPTIONAL_PROPS = self.saved_props[1]


class TestBrunchToolRun(BrunchToolWrapperTest):

    def test_base(self):
        t = tool.BrunchToolWrapper(self.store, self.product_path, '/my/tool')
        t.environment.clear()
        t.environment['PATH'] = 'abc:123'
        t.environment['BAZ'] = 'B "A" R'

        command = self.stub_subprocess.AddCommand(ret_code=0)
        t.run(['arg1'])
        command.AssertCallWas(['/my/tool', 'arg1'], shell=False, cwd=None,
                              stdout=None, stderr=None,
                              env={'PATH': 'abc:123', 'BAZ': 'B "A" R'})

        command = self.stub_subprocess.AddCommand(ret_code=1)
        with self.assertRaises(tool.ReturnError) as e:
            t.run(['arg1'])
        self.assertEqual(1, e.exception.errno)
        command.AssertCallWas(['/my/tool', 'arg1'], shell=False, cwd=None,
                              stdout=None, stderr=None,
                              env={'PATH': 'abc:123', 'BAZ': 'B "A" R'})


    def test_sigabrt(self):
        t = tool.BrunchToolWrapper(self.store, self.product_path, '/my/tool')
        self.stub_subprocess.AddCommand(['/my/tool', 'arg1'], ret_code=-6)

        with self.assertRaises(tool.ReturnError) as e:
            t.run(['arg1'])
        self.assertEqual(128 + 6, e.exception.errno)

    def test_cwd(self):
        t = tool.BrunchToolWrapper(self.store, self.product_path, '/my/tool')
        t.set_cwd('/here')
        command = self.stub_subprocess.AddCommand(['/my/tool'])

        t.run(['arg1'])
        command.AssertCallContained(cwd='/here')


class TestBrunchToolEnvironment(BrunchToolWrapperTest):

    def test_baseline(self):
        t = tool.BrunchToolWrapper(self.store, self.product_path, '/my/tool')
        t.environment.clear()
        t.environment['PATH'] = 'abc:123'
        t.environment['BAZ'] = 'B "A" R'
        command = self.stub_subprocess.AddCommand()

        t.run(['arg1'])
        command.AssertCallWas(['/my/tool', 'arg1'], shell=False, cwd=None,
                              stdout=None, stderr=None,
                              env={'PATH': 'abc:123', 'BAZ': 'B "A" R'})

    def test_no_path(self):
        """Ensures no exceptions if there is no PATH."""
        t = tool.BrunchToolWrapper(self.store, self.product_path, '/my/tool')
        command = self.stub_subprocess.AddCommand(['/my/tool', 'arg1'])

        t.run(['arg1'])
        self.assertEqual(self.stub_util.GetBDKPath('cli'),
                         command.GetCallArgs()[1]['env']['PATH'])

    def test_untouchable(self):
        """Tests config/bdk/allowed_environ limits."""
        self.store.bdk.allowed_environ = 'BDK_PATH'
        tool.os.environ['BDK_PATH'] = '/path/to/crazy/town'

        t = tool.BrunchToolWrapper(self.store, self.product_path, '/my/tool')
        command = self.stub_subprocess.AddCommand(['/my/tool', 'arg1'])

        t.run(['arg1'])
        self.assertEqual(self.stub_util.DEPRECATED_GetDefaultOSPath(),
                         command.GetCallArgs()[1]['env']['BDK_PATH'])

    def test_passthrough(self):
        """Ensures config/bdk/allowed_environ is respected."""
        self.store.bdk.allowed_environ = 'FUZZY WUZZY'
        tool.os.environ['FUZZY'] = 'WUZ'
        tool.os.environ['WUZZY'] = 'A BEAR'

        t = tool.BrunchToolWrapper(self.store, self.product_path, '/my/tool')
        command = self.stub_subprocess.AddCommand(['/my/tool', 'arg1'])

        t.run(['arg1'])
        call_env = command.GetCallArgs()[1]['env']
        self.assertEqual(call_env['FUZZY'], 'WUZ')
        self.assertEqual(call_env['WUZZY'], 'A BEAR')
        self.assertEqual(call_env['BDK_PATH'],
                         self.stub_util.DEPRECATED_GetDefaultOSPath())

    def test_product_out(self):
        """Ensures ANDROID_PRODUCT_OUT meets the contract from adb."""
        t = tool.BrunchToolWrapper(self.store, self.product_path, '/my/tool')
        command = self.stub_subprocess.AddCommand(['/my/tool', 'arg1'])

        t.run(['arg1'])
        self.assertEqual(
            'product_path/out/out-somedevice/target/product/somedevice',
            command.GetCallArgs()[1]['env']['ANDROID_PRODUCT_OUT'])


class TestBrunchHostToolWrapper(BrunchToolWrapperTest):

    def test_path(self):
        t = tool.BrunchHostToolWrapper(self.store, self.product_path, 'tool')
        self.assertEqual(
            'product_path/out/out-somedevice/host/linux-x86/bin/tool',
            t.path())

        t = tool.BrunchHostToolWrapper(self.store, self.product_path,
                                       'fastboot', arch='win-mips')
        self.assertEqual(
            'product_path/out/out-somedevice/host/win-mips/bin/fastboot',
            t.path())


class TestBrunchTargetToolWrapper(BrunchToolWrapperTest):

    def test_path(self):
        t = tool.BrunchTargetToolWrapper(self.store, self.product_path, 'tool')
        self.assertEqual(
            'product_path/out/out-somedevice/target/product/somedevice/tool',
            t.path())

        t = tool.BrunchTargetToolWrapper(self.store, self.product_path,
                                         'provision-dev')
        self.assertEqual(
            ('product_path/out/out-somedevice/target/product/somedevice/'
             'provision-dev'),
            t.path())
