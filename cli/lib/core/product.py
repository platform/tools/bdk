#
# Copyright (C) 2015 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

"""Product Management."""


import os

from core import config
from core import product_templates
from core import util


class ProductCreatorError(Exception):
    pass


class ProductCreator(object):
    def __init__(self, name, vendor, device):
        """Initializes the ProductCreators internal properties"""
        self._name = name
        self._config = None
        self._device = device
        self._vendor = vendor

    def exists(self):
        """Returns true if a subdirectory with the product name exists"""
        try:
            return os.path.exists(self._name)
        except OSError:
            return False

    def create(self):
        """Creates a subdirectory for the product and creates templates"""
        self._create_empty()

        # Populate the envsetup.sh file.
        env = Environment(self._config, self._name)
        env.envsetup()

    def _init_config(self):
        # Populate the configuration store
        self._config = config.ProductFileStore(os.path.abspath(self._name))
        self._config.name = self._name
        self._config.brand = 'Brillo'
        self._config.manufacturer = self._vendor
        self._config.manufacturer = self._vendor
        self._config.device = self._device
        self._config.bdk.buildtype = 'userdebug'
        self._config.bdk.version = util.GetBDKVersion()
        self._config.copy_files = """
# Format:
# path-in-product-dir:path-to-install-in-device
# E.g., weaved.conf:system/etc/weaved/weaved.conf
        """
        self._config.brillo.product_id = (
            'developer-boards:brillo-starter-board-<REPLACE ME>')
        self._config.brillo.crash_server = (
            'https://clients2.google.com/bc/report')
        # Building without Java isn't completely supported yet.
        # b/25281898
        self._config.bdk.java = '1'

    def _create_common(self):
        paths = [self._name, os.path.join(self._name, 'out'),
                 os.path.join(self._name, 'sepolicy'),
                 os.path.join(self._name, 'src')]
        for path in paths:
            if not os.path.exists(path):
                os.makedirs(path)
        self._init_config()

        with open(os.path.join(self._name, 'AndroidProducts.mk'), 'w') as f:
            f.write(product_templates.ANDROIDPRODUCTS_MK.substitute(
                self._config.dict()))

    def _create_empty(self):
        """Creates a subdirectory for the product and creates templates"""
        self._create_common()
        with open(os.path.join(self._name, ('%s.mk' % self._name)), 'w') as f:
            f.write(
                product_templates.PRODUCT_MK.substitute(self._config.dict()))

        # SELinux templates.
        sepolicy_dir = os.path.join(self._name, 'sepolicy')
        service_te_file = os.path.join(sepolicy_dir,
                                       '%s_service.te' % self._name)

        with open(service_te_file, 'w') as f:
            f.write(
                product_templates.SELINUX_DOMAIN.substitute(
                    self._config.dict()))

        with open(os.path.join(sepolicy_dir, 'file_contexts'), 'w') as f:
            f.write(product_templates.SELINUX_FILE_CONTEXTS.substitute(
                self._config.dict()))


class Environment(object):
    """This class is used to generate a shell environment meant to streamline
    the use of the BDK while keeping it familiar to Android device developers.

    Note: None of the templates other than ENVSETUP should contain single
        quotes ('). If they do, it will break during sourcing of envsetup.sh.
    """

    def __init__(self, config_, product_dir=util.GetProductDir()):
        self._config = config_
        self._product_dir = product_dir

    def banner(self):
        """Return the banner to be printed after envsetup.sh is sourced."""
        bdk_path = util.GetBDKPath()
        bdk_version = util.GetBDKVersion()
        # TODO(b/25952629) Add support for a BDK warning if an update is needed.
        bdk_warning = ''
        return product_templates.BDK_BANNER.substitute(
            self._config.dict(),
            bdk_version=bdk_version,
            bdk_path=bdk_path,
            bdk_warning=bdk_warning)

    def exports(self):
        """Export shell environment variables for calling dev tools"""
        bdk_path = util.DEPRECATED_GetDefaultOSPath()
        return product_templates.ENV_EXPORTS.substitute(
            self._config.dict(),
            bdk_path=bdk_path,
            product_path=self._product_dir,
            cli_path=util.GetBDKPath('cli'),
            target_out=os.path.join(self._product_dir, 'out',
                                    'out-%s' % (self._config.device),
                                    'target', 'product', self._config.device)
        )

    def aliases(self):
        """Wrap bdk commands with shell functions"""
        return product_templates.ENV_ALIASES.substitute(
            self._config.dict(), product_path=self._product_dir)

    def environment(self):
        """Returns a complete shell environment for product development"""
        return self.banner() + self.exports() + self.aliases()

    def envsetup(self):
        """Generates a file which will run bdk product envsetup"""
        bdk_path = util.DEPRECATED_GetDefaultOSPath()
        tools_path = util.GetBDKPath()
        with open(os.path.join(self._product_dir, 'envsetup.sh'), 'w') as f:
            f.write(product_templates.ENVSETUP.substitute(
                self._config.dict(), bdk_path=bdk_path,
                product_path=self._product_dir,
                tools_path=tools_path))
