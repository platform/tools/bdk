#
# Copyright (C) 2016 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

"""core.util module stubs."""


import os

from core import util
import error


class StubUtil(object):
    """Stubs for our core.util module."""

    class Error(error.Error):
        pass

    class HostUnsupportedArchError(Error):
        pass

    class OSVersionError(Error):
        pass

    PROJECT_SPEC_FILENAME = 'unittest_project_spec.xml'

    def __init__(self, bdk_path='/some/bdk/path',
                 user_data_path='/user/data/path',
                 bdk_version='99.99', arch='fake_arch',
                 deprecated_os_path='/no/longer/a/thing'):
        self.bdk_path = bdk_path
        self.user_data_path = user_data_path
        self.bdk_version = bdk_version
        self.arch = arch
        self.arch_is_supported = True
        self.project_spec = None
        self.deprecated_os_path = deprecated_os_path

    def GetBDKPath(self, *relpath):
        return os.path.abspath(os.path.join(self.bdk_path, *relpath))

    def GetUserDataPath(self, *relpath):
        return os.path.join(self.user_data_path, *relpath)

    def GetBDKVersion(self):
        return self.bdk_version

    def DEPRECATED_GetDefaultOSPath(self, *relpath):
        return os.path.join(self.deprecated_os_path, *relpath)

    @classmethod
    def AsShellArgs(cls, ary):
        return util.AsShellArgs(ary)

    @classmethod
    def GetExitCode(cls, retval):
        if retval == 0:
            return 0
        else:
            return 1

    def GetHostArch(self):
        if not self.arch_is_supported:
            raise self.HostUnsupportedArchError(
                '{} is not supported.'.format(self.arch))
        return self.arch

    def FindProjectSpec(self):
        return self.project_spec
