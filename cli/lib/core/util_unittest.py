#
# Copyright (C) 2016 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#


"""Tests for util.py"""


import unittest

from core import util
from test import stubs


class UtilTest(unittest.TestCase):
    """Tests for the Util functions."""

    def setUp(self):
        self.stub_os = stubs.StubOs()
        self.stub_open = stubs.StubOpen(self.stub_os)

        util.os = self.stub_os
        util.open = self.stub_open.open

    def test_bdk_path(self):
        # We use startswith since it might also be .pyc if unit tests have been
        # previously run.
        self.assertTrue(__file__.startswith(
            util.GetBDKPath('cli', 'lib', 'core', 'util_unittest.py')))

    def test_bdk_version(self):
        self.stub_open.files[util.GetBDKPath('VERSION')] = (
            stubs.StubFile('123.45'))
        self.stub_os.path.should_exist = [util.GetBDKPath('VERSION')]
        self.assertEqual(util.GetBDKVersion(), '123.45')

    def test_get_product_dir(self):
        self.stub_os.cwd = '/long/way/down/to/the/bottom/turtle/'
        self.stub_os.path.should_exist = [
            self.stub_os.path.join('/long', util.ANDROID_PRODUCTS_MK),
            self.stub_os.path.join('/long/way/down/to/top',
                                   util.ANDROID_PRODUCTS_MK),
            self.stub_os.path.join('/long/way', util.ANDROID_PRODUCTS_MK)]
        # Make sure we find the closest in our direct path.
        self.assertEqual(util.GetProductDir(), '/long/way')

    def test_get_no_product_dir(self):
        self.stub_os.cwd = '/long/way/down/to/the/bottom/turtle/'
        self.stub_os.path.should_exist = [
            self.stub_os.path.join('/other', util.ANDROID_PRODUCTS_MK)]
        self.assertEqual(util.GetProductDir(), None)

    def test_get_project_spec(self):
        self.stub_os.cwd = '/long/way/down/to/the/bottom/turtle/'
        self.stub_os.path.should_exist = [
            self.stub_os.path.join('/long', util.PROJECT_SPEC_FILENAME),
            self.stub_os.path.join('/long/way/down/to/top',
                                   util.PROJECT_SPEC_FILENAME),
            self.stub_os.path.join('/long/way', util.PROJECT_SPEC_FILENAME)]
        # Make sure we find the closest in our direct path.
        self.assertEqual(util.FindProjectSpec(),
                         self.stub_os.path.join('/long/way',
                                                util.PROJECT_SPEC_FILENAME))

    def test_get_no_project_spec(self):
        self.stub_os.cwd = '/long/way/down/to/the/bottom/turtle/'
        self.stub_os.path.should_exist = [
            self.stub_os.path.join('/other', util.PROJECT_SPEC_FILENAME)]
        self.assertEqual(util.FindProjectSpec(), None)

    def test_get_host(self):
        self.stub_os.SetUname(('Linux', 'host_name.website.com',
                               '99.99-awesome', 'stuff', 'x86_64'))
        self.assertEqual(util.GetHostArch(), 'linux-x86')

    def test_get_bad_host(self):
        self.stub_os.SetUname(('OSX', 'host_name.website.com', '99.99-awesome',
                               'stuff', 'x86_64'))
        with self.assertRaises(util.HostUnsupportedArchError):
            util.GetHostArch()
