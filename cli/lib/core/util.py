#
# Copyright (C) 2015 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

"""Bdk util functions"""


import os
import sys

from core import timer
import error


class Error(error.Error):
    """Base error for util module."""


class HostUnsupportedArchError(Error):
    """Raised when run on an unsupported host architecture."""
    description = 'The BDK only supports Linux machines.'


class PathError(Error):
    """Raised when an expected path can't be found."""


class OSVersionError(Error):
    """Raised when an invalid OS version is requested."""


ANDROID_PRODUCTS_MK = 'AndroidProducts.mk'
PROJECT_SPEC_FILENAME = 'project.xml'


def GetBDKPath(*relpath_args):
    """Find a path relative to the base of the BDK."""
    # We know this file is located in <bdk>/cli/lib/core/.
    core_path = os.path.dirname(__file__)
    return os.path.abspath(os.path.join(core_path, '..', '..', '..',
                                        *relpath_args))


def GetUserDataPath(*relpath_args):
    """Find a path relative to the user data dir."""
    # User data is stored at <bdk>/user_data.
    return GetBDKPath('user_data', *relpath_args)


def GetBDKVersion():
    """Find the BDK version"""
    # Cache the version.
    # TODO(wad)(b/25952145) make this less hacky.
    if vars().get('bdk_version') is not None:
        return vars().get('bdk_version')
    version_path = GetBDKPath('VERSION')
    version = 0  # Default version.
    with open(version_path, 'r') as f:
        version = f.readline().strip()
    vars()['bdk_version'] = version
    return version


def DEPRECATED_GetDefaultOSPath(*relpath):
    """DEPRECATED - use <platform>.os.path.

    For use by legacy brunch commands without a target platform.
    Not compatible with out-of-tree BDK.
    """
    # Assume we're using in-tree BDK, which is located
    # at <os_path>/tools/bdk.
    return GetBDKPath('..', '..', *relpath)


def GetProductDir():
    """Walks from cwd upward to find the product root"""
    p = os.getcwd()
    while p != '/':
        if os.path.isfile(os.path.join(p, ANDROID_PRODUCTS_MK)):
            return p
        p = os.path.dirname(p)
    return None


def FindProjectSpec():
    """Walks from cwd upward to find the project spec."""
    p = os.getcwd()
    while p != '/':
        f = os.path.join(p, PROJECT_SPEC_FILENAME)
        if os.path.isfile(f):
            return f
        p = os.path.dirname(p)
    return None


def AsShellArgs(args):
    # TODO(wad)(b/25952900) we should sanitize beyond just quotes.
    return ' '.join(['"%s"' % a.replace('"', '\\"') for a in args])


def GetExitCode(status):
    """Convert an os.wait status code to a shell return value"""
    if os.WIFEXITED(status):
        return os.WEXITSTATUS(status)
    if os.WIFSTOPPED(status):
        return 128+os.WSTOPSIG(status)
    if os.WIFSIGNALED(status):
        return 128+os.WTERMSIG(status)
    return 1


def GetUserInput(prompt):
    """Prompt a user for input and return the result.

    Also pauses all known running timers, so that their
    time doesn't include user thinking time.
    """
    print prompt
    sys.stdout.flush()
    timer.Timer.MassPauseRunningTimers()
    result = sys.stdin.readline()
    timer.Timer.ResumeMassPausedTimers()
    return result


def GetHostArch():
    """Returns the Android name of the host architecture.

    Raises:
        HostError: host architecture is not supported.
    """
    if os.uname()[0].lower() != 'linux':
        raise HostUnsupportedArchError()
    return 'linux-x86'
