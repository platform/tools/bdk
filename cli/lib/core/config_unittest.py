#
# Copyright (C) 2015 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

"""Unittests for classes in config.py"""


import string
import unittest

from core import config
from test import stubs


class ProductFileStoreTest(unittest.TestCase):
    def setUp(self):
        self.product_path = 'product_path'
        stub_os = stubs.StubOs()
        self.stub_open = stubs.StubOpen(stub_os)
        config.open = self.stub_open.open
        config.os = stub_os
        self.store = config.ProductFileStore(self.product_path)

    def test_load_base(self):
        brand_path = 'product_path/config/brand'
        self.stub_open.files[brand_path] = stubs.StubFile('my-brand')
        config.os.path.should_exist.append(brand_path)
        self.assertEqual(self.store.brand, 'my-brand')

    def test_load_deep(self):
        version_path = 'product_path/config/bdk/version'
        expected = 'veggies.1.2.3.4'
        self.stub_open.files[version_path] = stubs.StubFile(expected)
        config.os.path.should_exist.append(version_path)
        self.assertEqual(self.store.bdk.version, expected)

    def test_load_comments(self):
        expected = 'veggies.1.2.3.4'
        input_ = """
# Current version: name.num.bers
%s
# That's it!
""" % expected
        f = stubs.StubFile(input_)
        self.stub_open.files['product_path/config/bdk/version'] = f
        config.os.path.should_exist = ['product_path/config/bdk',
                                       'product_path/config/bdk/version']
        self.assertEqual(self.store.bdk.version, expected)
        self.assertEqual(f.contents, string.split(input_, '\n'))

    def test_load_multiline(self):
        expected = 'ledflasher  ledservice'
        input_ = """
ledflasher

ledservice
"""
        f = stubs.StubFile(input_)
        self.stub_open.files['product_path/config/packages'] = f
        config.os.path.should_exist = ['product_path/config',
                                       'product_path/config/packages']
        self.assertEqual(self.store.packages, expected)
        self.assertEqual(f.contents, string.split(input_, '\n'))

    def test_load_invalid(self):
        with self.assertRaises(AttributeError):
            _ = self.store.no.real.thing

    def test_save_exists(self):
        f = stubs.StubFile()
        self.stub_open.files['product_path/config/bdk/version'] = f
        config.os.path.should_exist += ['product_path/config/bdk',
                                        'product_path/config/bdk/version']
        expected = 'everything awesome'
        self.store.bdk.version = expected
        self.assertEqual(f.contents, [expected])

    def test_save_makedirs_fail(self):
        config.os.path.should_exist = []
        with self.assertRaises(OSError):
            self.store.bdk.version = 'dontmatter'

    def test_save_makedirs_works(self):
        f = stubs.StubFile()
        self.stub_open.files['product_path/config/bdk/version'] = f
        config.os.should_makedirs = ['product_path/config/bdk']
        expected = 'everything awesome'
        self.store.bdk.version = expected
        self.assertEqual(f.contents, [expected])

    def test_save_valid_buildtype(self):
        f = stubs.StubFile()
        self.stub_open.files['product_path/config/bdk/buildtype'] = f
        config.os.should_makedirs = ['product_path/config/bdk']
        expected = 'userdebug'
        with self.assertRaises(ValueError):
            self.store.bdk.buildtype = 'not legit'
        self.store.bdk.buildtype = expected
        self.assertEqual(f.contents, [expected])

    def test_save_load_comments(self):
        f = stubs.StubFile()
        self.stub_open.files['product_path/config/packages'] = f
        config.os.path.should_exist = ['product_path/config',
                                       'product_path/config/packages']
        input_ = """
# Running service:
ledservice
# Debugging app
ledprobe
"""
        self.store.packages = input_
        self.assertEqual(string.join(f.contents, '\n'), input_)
        expected = 'ledservice ledprobe'
        self.assertEqual(self.store.packages, expected)

    def test_dict(self):
        f = stubs.StubFile()
        self.stub_open.files['product_path/config/packages'] = f
        config.os.path.should_exist = ['product_path/config',
                                       'product_path/config/packages']
        input_ = """
# Running service:
ledservice
# Debugging app
ledprobe
"""
        self.store.packages = input_
        self.assertEqual(string.join(f.contents, '\n'), input_)
        expected = 'ledservice ledprobe'
        self.assertEqual(self.store.dict()['packages'], expected)
