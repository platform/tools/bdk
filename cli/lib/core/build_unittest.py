#
# Copyright (C) 2016 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

"""Unit tests for build.py."""


import os
import unittest

from core import build
from core import util_stub
from project import platform_stub
from test import stubs


class BuildPlatformTest(unittest.TestCase):
    # Valid arguments listed here so we can test changing one at a time.
    _BSP = 'test_board'
    _BUILD_TYPE = 'user'
    _OUT_DIR = '/foo/bar'
    _OS_VERSION = '12.34'
    _LOG_FILE = os.path.join(_OUT_DIR, 'bdk_last_build.log')

    def setUp(self):
        self.stub_os = stubs.StubOs()
        self.stub_util = util_stub.StubUtil()
        self.stub_subprocess = stubs.StubSubprocess()

        # Allow BuildPlatform() to make the directory for the log file.
        self.stub_os.should_makedirs = [self._OUT_DIR]

        build.os = self.stub_os
        build.util = self.stub_util
        build.subprocess = self.stub_subprocess

        self.platform = platform_stub.StubPlatform(
            board=self._BSP, build_type=self._BUILD_TYPE,
            os_version=self._OS_VERSION, build_cache=self._OUT_DIR,
            should_link=True)

    def test_success(self):
        make_command = self.stub_subprocess.AddCommand()
        tee_command = self.stub_subprocess.AddCommand()

        self.assertEqual(0, build.BuildPlatform(self.platform))

        # pylint: disable=protected-access
        make_command.AssertCallWas(
            build._GetBuildScript(self.platform),
            stdout=self.stub_subprocess.PIPE,
            stderr=self.stub_subprocess.STDOUT,
            executable='bash', shell=True, env={})
        tee_command.AssertCallWas(['tee', self._LOG_FILE],
                                  stdin=make_command.stdout, env={})

    def test_success_extra_make_args(self):
        """Tests passing additional make args from the user."""
        extra_make_args = ['-j', '40']
        make_command = self.stub_subprocess.AddCommand()
        self.stub_subprocess.AddCommand(['tee', self._LOG_FILE])

        self.assertEqual(0, build.BuildPlatform(
            self.platform, extra_make_args=extra_make_args))

        # pylint: disable=protected-access
        make_command.AssertCallWas(
            build._GetBuildScript(
                self.platform, extra_make_args=extra_make_args),
            stdout=self.stub_subprocess.PIPE,
            stderr=self.stub_subprocess.STDOUT,
            executable='bash', shell=True, env={})

    def test_success_env_passthrough(self):
        """Tests passing needed environmental variables through."""
        self.stub_os.environ['USER'] = 'test_user'
        self.stub_os.environ['PATH'] = 'test_path'
        make_command = self.stub_subprocess.AddCommand()
        tee_command = self.stub_subprocess.AddCommand(['tee', self._LOG_FILE])

        self.assertEqual(0, build.BuildPlatform(
            self.platform, self._OUT_DIR))

        # Make sure USER passes through but PATH does not.
        make_command.AssertCallContained(env={'USER': 'test_user'})
        tee_command.AssertCallContained(env={'USER': 'test_user'})

    def test_return_exit_code(self):
        """Tests that the make exit code is returned."""
        self.stub_subprocess.AddCommand(ret_code=1)
        self.stub_subprocess.AddCommand(['tee', self._LOG_FILE])

        self.assertEqual(1, build.BuildPlatform(
            self.platform, self._OUT_DIR))

    @staticmethod
    def _raise_oserror():
        raise OSError('a faked OSError has occurred.')

    def test_missing_bash(self):
        """Tests passing additional make args from the user."""
        # Expect a make command.
        self.stub_subprocess.AddCommand(init_side_effect=self._raise_oserror)
        with self.assertRaises(build.BuildUtilityError):
            build.BuildPlatform(self.platform, self._OUT_DIR)

    def test_missing_tee(self):
        """Tests passing additional make args from the user."""
        # Expect a make command.
        self.stub_subprocess.AddCommand()
        self.stub_subprocess.AddCommand(['tee', self._LOG_FILE],
                                        init_side_effect=self._raise_oserror)
        with self.assertRaises(build.BuildUtilityError):
            build.BuildPlatform(self.platform, self._OUT_DIR)
