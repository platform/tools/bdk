#
# Copyright (C) 2016 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#


"""Unittests for classes in user_config.py."""


import unittest

from core import user_config
from core import util_stub
from test import stubs


class UserConfigTest(unittest.TestCase):
    def setUp(self):
        self.stub_os = stubs.StubOs()
        self.stub_util = util_stub.StubUtil()
        self.stub_uuid = stubs.StubUuid()

        user_config.os = self.stub_os
        user_config.util = self.stub_util
        user_config.uuid = self.stub_uuid

        self.user_config = user_config.UserConfig(file_path=':memory:')

    def test_default_config(self):
        uc = user_config.UserConfig()
        # Default user config should be in user data dir.
        # pylint: disable=protected-access
        self.assertTrue(uc._path.startswith(self.stub_util.user_data_path))

    def test_initialize_opt_in(self):
        self.user_config.initialize(opt_in=True)
        self.assertEqual(self.user_config.metrics_opt_in, '1')
        self.assertEqual(self.user_config.uid, self.stub_uuid.generate)

    def test_initialize_opt_out(self):
        self.user_config.initialize(opt_in=False)
        self.assertEqual(self.user_config.metrics_opt_in, '0')
        self.assertEqual(self.user_config.uid, self.stub_uuid.generate)

    def test_opt_in_values(self):
        self.user_config.metrics_opt_in = '0'
        self.assertEqual(self.user_config.metrics_opt_in, '0')
        self.user_config.metrics_opt_in = '1'
        self.assertEqual(self.user_config.metrics_opt_in, '1')
        with self.assertRaises(ValueError):
            self.user_config.metrics_opt_in = '2'
        with self.assertRaises(ValueError):
            self.user_config.metrics_opt_in = ''
        with self.assertRaises(ValueError):
            self.user_config.metrics_opt_in = '-1'
        with self.assertRaises(ValueError):
            self.user_config.metrics_opt_in = 'not_a_number'
        with self.assertRaises(ValueError):
            self.user_config.metrics_opt_in = None
        # Must be given in string form.
        with self.assertRaises(ValueError):
            self.user_config.metrics_opt_in = 1
        with self.assertRaises(ValueError):
            self.user_config.metrics_opt_in = 0

    def test_defaults(self):
        self.assertIsNone(self.user_config.metrics_opt_in)
        self.assertIsNone(self.user_config.uid)
        self.assertEqual(self.user_config.bsp_dir,
                         self.stub_util.GetBDKPath('BSPs'))
        self.assertEqual(self.user_config.platform_cache,
                         self.stub_util.GetBDKPath('platform_cache'))
        self.assertEqual(self.user_config.os_root,
                         self.stub_util.DEPRECATED_GetDefaultOSPath())

        # Set values (except for metrics_opt_in, tested above).
        self.user_config.uid = 'uid!'
        self.assertEqual(self.user_config.uid, 'uid!')
        self.user_config.bsp_dir = 'bsp_dir!'
        self.assertEqual(self.user_config.bsp_dir, 'bsp_dir!')
        self.user_config.platform_cache = 'platform_cache!'
        self.assertEqual(self.user_config.platform_cache, 'platform_cache!')
        self.user_config.os_root = 'os_root!'
        self.assertEqual(self.user_config.os_root, 'os_root!')

        # Reset values.
        self.user_config.uid = None
        self.assertIsNone(self.user_config.uid)
        self.user_config.bsp_dir = None
        self.assertEqual(self.user_config.bsp_dir,
                         self.stub_util.GetBDKPath('BSPs'))
        self.user_config.platform_cache = None
        self.assertEqual(self.user_config.platform_cache,
                         self.stub_util.GetBDKPath('platform_cache'))
        self.user_config.os_root = None
        self.assertEqual(self.user_config.os_root,
                         self.stub_util.DEPRECATED_GetDefaultOSPath())

        # Empty string should behave like default for those with defaults.
        self.user_config.uid = ''
        self.assertEqual(self.user_config.uid, '')
        self.user_config.bsp_dir = ''
        self.assertEqual(self.user_config.bsp_dir,
                         self.stub_util.GetBDKPath('BSPs'))
        self.user_config.platform_cache = ''
        self.assertEqual(self.user_config.platform_cache,
                         self.stub_util.GetBDKPath('platform_cache'))
        self.user_config.os_root = ''
        self.assertEqual(self.user_config.os_root,
                         self.stub_util.DEPRECATED_GetDefaultOSPath())
