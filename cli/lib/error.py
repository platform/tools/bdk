#
# Copyright (C) 2016 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#


"""Bdk error classes."""


GENERIC_ERRNO = 1


class Error(Exception):
    """A root class for all BDK errors."""

    description = 'BDK Error'

    def __init__(self, message='', errno=GENERIC_ERRNO):
        if errno == 0:
            raise ValueError('Error numbers can not be 0.')

        full_message = self.description
        if message is not None:
            # Just in case someone passes a non-string.
            message = str(message)
        if message:
            full_message += ': ' + message
        super(Error, self).__init__(full_message)
        self.errno = errno
