#
# Copyright (C) 2015 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

"""Unittests for classes in manifest.py"""

import json
import unittest

from bsp import device_stub
from bsp import manifest
from bsp import operating_system_stub
from test import stubs

class ManifestTest(unittest.TestCase):
    def setUp(self):
        self.stub_os = stubs.StubOs()
        self.stub_open = stubs.StubOpen(self.stub_os)
        self.stub_operating_system = (
            operating_system_stub.StubOperatingSystemModule())

        self.base_manifest = {
            'packages': {
                'package_1': {
                    'package_type': 'git',
                    'remote': 'remote',
                    'version': 'version',
                    'subpackages': {
                        'subpackage_1': {
                            'subdir': '.',
                            'licenses': ['license1', 'license2']
                        }
                    }
                }
            },
            'devices': {
                'device_1': {
                    'device_name': 'Test Device 1',
                    'vendor': 'test_vendor',
                    'arch': 'test_arch',
                    'packages': {
                        'package_1': {
                            'subpackage_1': 'path/to/extract'
                        }
                    }
                }
            }
        }

        manifest.os = self.stub_os
        manifest.open = self.stub_open.open
        manifest.operating_system = self.stub_operating_system

        self.manifest_path = 'manifest'
        self.stub_os.path.should_exist.append(self.manifest_path)

    def test_init(self):
        packages = {'a' : 'b'}
        devices = {'c' : 'd'}
        man = manifest.Manifest(packages, devices)
        self.assertEqual(man.packages, packages)
        self.assertEqual(man.devices, devices)
        # Should get a default 'brillo' OS available.
        self.assertIn('brillo', man.operating_systems)

    def test_from_dict(self):
        man = manifest.Manifest.from_dict(self.base_manifest)
        self.assertIsInstance(man, manifest.Manifest)

    def test_from_dict_incomplete_devices(self):
        incomplete_manifest = self.base_manifest
        incomplete_manifest['devices']['device_2'] = {}
        with self.assertRaisesRegexp(KeyError, 'device_2'):
            manifest.Manifest.from_dict(incomplete_manifest)

    def test_from_dict_incomplete_packages(self):
        incomplete_manifest = self.base_manifest
        incomplete_manifest['packages']['package_2'] = {}
        with self.assertRaisesRegexp(KeyError, 'package_2'):
            manifest.Manifest.from_dict(incomplete_manifest)

    def test_from_dict_missing_devices(self):
        incomplete_manifest = self.base_manifest
        del incomplete_manifest['devices']
        with self.assertRaises(KeyError):
            manifest.Manifest.from_dict(incomplete_manifest)

    def test_from_dict_missing_packages(self):
        incomplete_manifest = self.base_manifest
        del incomplete_manifest['packages']
        with self.assertRaises(KeyError):
            manifest.Manifest.from_dict(incomplete_manifest)

    def test_minimal_manifest(self):
        minimal_manifest = {'packages': {}, 'devices': {}}
        man = manifest.Manifest.from_dict(minimal_manifest)
        self.assertIsInstance(man, manifest.Manifest)
        self.assertEqual(len(man.packages), 0)
        self.assertEqual(len(man.devices), 0)

    def test_basic_json_manifest(self):
        self.stub_open.files[self.manifest_path] = (
            stubs.StubFile(json.dumps(self.base_manifest)))
        man = manifest.Manifest.from_json(self.manifest_path)
        self.assertIsInstance(man, manifest.Manifest)

    def test_non_json_manifest(self):
        self.stub_open.files[self.manifest_path] = stubs.StubFile(
            '{ unmatched bracket is invalid json')
        with self.assertRaisesRegexp(ValueError, 'json'):
            manifest.Manifest.from_json(self.manifest_path)

    def test_incomplete_json_manifest(self):
        incomplete_manifest = self.base_manifest
        incomplete_manifest['devices']['device_2'] = {}
        self.stub_open.files[self.manifest_path] = (
            stubs.StubFile(json.dumps(incomplete_manifest)))
        # Hopefully the error indicates that the issue is somewhere in device_2
        with self.assertRaisesRegexp(KeyError, 'device_2'):
            manifest.Manifest.from_json(self.manifest_path)

    def test_invalid_json_manifest(self):
        invalid_manifest = self.base_manifest
        invalid_manifest['devices']['device_1'][
            'packages']['package_1']['subpackage_2'] = 'path'
        self.stub_open.files[self.manifest_path] = (
            stubs.StubFile(json.dumps(invalid_manifest)))
        # Error should indicate that subpackage_2 is not a defined subpackage of
        # package_1.
        with self.assertRaisesRegexp(ValueError, 'subpackage_2'):
            manifest.Manifest.from_json(self.manifest_path)

    def test_json_manifest_not_found(self):
        self.stub_os.path.should_exist = []
        # Some sort of error about the file.
        with self.assertRaisesRegexp(IOError, self.manifest_path):
            manifest.Manifest.from_json(self.manifest_path)

    def test_bsp_is_available(self):
        # Set up the manifest with stubbed devices.
        devices = {'device_1': device_stub.StubDevice(downloaded=True),
                   'device_2': device_stub.StubDevice(downloaded=False)}

        man = manifest.Manifest([], devices)

        # Check that an unknown BSP name raises an error.
        with self.assertRaises(manifest.UnknownBspError):
            man.is_bsp_available('invalid_bsp_name')

        # Check the return values are correct.
        self.assertTrue(man.is_bsp_available('device_1'))
        self.assertFalse(man.is_bsp_available('device_2'))
