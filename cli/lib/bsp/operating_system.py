#
# Copyright (C) 2016 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#


"""Provides a class for Operating Systems."""


import os
import re

from core import user_config
from core import util
import error


VERSION_RE = r'^[0-9]+\.[0-9]+\.[0-9]+$'


class Error(error.Error):
    description = 'Error with OS'


class VersionError(Error):
    """Raised when an OS version is invalid."""
    description = 'Invalid OS version'


class OperatingSystem(object):
    """Class to represent versioned device operating systems.

    Used to find various files when building against a specific
    version of Brillo (or other OS). Does not contain
    device-specific code; that must be downloaded via a BSP
    and combined with an OS through a Platform.

    For now, an OS has a name, version, and build tree. The
    latter will eventually be replaced by prebuilts.

    Attributes:
        name: the name of the OS.
        root: the root of the OS tree.
        version: the version of the OS.
    """
    def __init__(self, name, root):
        self._name = name
        self._root = os.path.abspath(root)
        # For now, we read version from the VERSION file
        # at <root>/tools/bdk/VERSION. Eventually this will
        # just be a property like in Device.
        version_path = self.path('tools', 'bdk', 'VERSION')
        try:
            with open(version_path) as f:
                self._version = f.read().strip()
        except IOError as e:
            raise VersionError(e)
        if not re.match(VERSION_RE, self._version):
            raise VersionError('{} (version must match regex "{}")'.format(
                self._version, VERSION_RE))

    def __repr__(self):
        return '{}.{}'.format(self.name, self.version)

    @property
    def name(self):
        return self._name

    @property
    def root(self):
        return self._root

    @property
    def version(self):
        return self._version

    def path(self, *relpath):
        """Find a path relative to the OS tree."""
        return os.path.join(self.root, *relpath)

    def is_available(self):
        """Return True if the OS is available."""
        # We assume if the dir is there, the OS is available.
        # Since init depends on a file under root, this should
        # always be true for now.
        return os.path.isdir(self.root)


def init_default_operating_system():
    """Gets a new object representing the default OS."""
    return OperatingSystem('brillo', user_config.USER_CONFIG.os_root)
