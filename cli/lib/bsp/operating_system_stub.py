#
# Copyright (C) 2016 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#


"""Stubs for classes in operating_system.py"""


import os


class StubOperatingSystem(object):
    def __init__(self, name='', root='', version='', available=False):
        self.name = name
        self.root = root
        self.version = version
        self.available = available

    def path(self, *relpath):
        return os.path.join(self.root, *relpath)

    def is_available(self):
        return self.available


class StubOperatingSystemModule(object):
    class Error(Exception):
        """Raised when a stub is supposed to error."""

    def __init__(self, os_name='brillo', os_version='0.0.0', os_path='/os_path',
                 os_available=False, raise_on_init=False):
        self.os_name = os_name
        self.os_version = os_version
        self.os_path = os_path
        self.os_available = os_available
        self.raise_on_init = raise_on_init

    def OperatingSystem(self, *args, **kwargs):
        if self.raise_on_init:
            raise self.Error('OS failed to init by request')
        return StubOperatingSystem(*args, **kwargs)

    def init_default_operating_system(self):
        return self.OperatingSystem(name=self.os_name, root=self.os_path,
                                    version=self.os_version,
                                    available=self.os_available)
