#
# Copyright (C) 2015 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
"""Provides a class for BSP subpackages."""


SUBPACKAGE_KEY_SUBDIR = 'subdir'
SUBPACKAGE_KEY_LICENSES = 'licenses'


class Subpackage(object):
    """Class for subpackages.

    Subpackages are just dumb data.

    Attributes:
        name: the name of the subpackage.
        subdir: the subdir of the remote package to extract.
        licenses: list of license file paths, relative to subdir.
    """

    def __init__(self, name, subdir, licenses):
        self.name = name
        self.subdir = subdir
        self.licenses = licenses

    @classmethod
    def from_dict(cls, dic, name):
        """Create a SubPackage from a dict.

        Args:
            dic: the dictionary to build the subpackage from.
            name: the name of the subpackage.

        Returns:
            A subpackage named |name| generated from |dic|.
        """
        return cls(name,
                   dic[SUBPACKAGE_KEY_SUBDIR],
                   dic[SUBPACKAGE_KEY_LICENSES])
