#
# Copyright (C) 2015 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

"""Unittests for classes in subpackage.py"""

import unittest

from bsp import subpackage

class SubpackageTest(unittest.TestCase):
    def setUp(self):
        self.base_subpackage = {
            'subdir': 'path/to/subdir',
            'licenses': ['license1', 'license2']
        }


    def test_from_dict(self):
        subpkg = subpackage.Subpackage.from_dict(self.base_subpackage,
                                                 'subpackage')
        self.assertIsInstance(subpkg, subpackage.Subpackage)
        self.assertEqual(subpkg.name, 'subpackage')
        self.assertEqual(subpkg.subdir, 'path/to/subdir')
        self.assertEqual(subpkg.licenses, ['license1', 'license2'])

    def test_from_dict_noisy(self):
        noisy_subpackage = self.base_subpackage
        noisy_subpackage['not_a_real_key'] = 'not a real value'
        subpkg = subpackage.Subpackage.from_dict(noisy_subpackage, 'subpackage')
        self.assertIsInstance(subpkg, subpackage.Subpackage)
        self.assertEqual(subpkg.name, 'subpackage')
        self.assertEqual(subpkg.subdir, 'path/to/subdir')
        self.assertEqual(subpkg.licenses, ['license1', 'license2'])

    def test_from_dict_incomplete(self):
        incomplete_subpackage = self.base_subpackage
        del incomplete_subpackage['subdir']
        self.assertRaisesRegexp(KeyError, 'subdir',
                                subpackage.Subpackage.from_dict,
                                incomplete_subpackage, 'subpackage')
