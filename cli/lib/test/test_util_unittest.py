#
# Copyright (C) 2016 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

"""Unit tests for test_util.py."""


from __future__ import print_function
import sys
import unittest

from test import test_util


class OutputCaptureTest(unittest.TestCase):

    def test_manual_replace(self):
        """Tests manually replacing and restoring after."""
        output = test_util.OutputCapture()
        output.replace_sys_streams()

        print('foo')
        print('bar', file=sys.stderr)

        self.assertEqual('foo\n', output.stdout)
        self.assertEqual('bar\n', output.stderr)

        output.restore_sys_streams()

    def test_context_manager(self):
        """Tests using a context manager."""
        with test_util.OutputCapture() as output:
            print('foo')
            print('bar', file=sys.stderr)

        self.assertEqual('foo\n', output.stdout)
        self.assertEqual('bar\n', output.stderr)

    def test_context_manager_exception(self):
        """Tests raising an exception from inside the context manager.

        The exception should be propagated out of the context manager and
        the previous sys streams should be restored.
        """
        initial_streams = sys.stdout, sys.stderr

        with self.assertRaises(ValueError):
            with test_util.OutputCapture():
                raise ValueError('foo')

        self.assertEqual(initial_streams, (sys.stdout, sys.stderr))
