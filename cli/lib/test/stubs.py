#
# Copyright (C) 2015 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

"""Stubs for mocking out built-in functionality for tests.

This file should only contain stubs meant to replace Python built-in
modules and functions. Stubs for our own modules should live in
separate files next to the real implementation.

Generally to use these stubs, you want to replace a reference to an
imported module with its corresponding stub object, for example:
    module_under_test.os = stubs.StubOs()

Each stub class should provide functions mocking out any required
function for the built-in module. Stubs may also provide additional
functions or attributes in order to configure the behavior.

Avoid using global or class variables when implementing stubs; these
will carry over between tests and can cause unexpected dependencies
and test flake. All state should be contained in member variables,
and each test should create new stub objects to make sure they are
starting with a clean slate.
"""


import os
import string
import StringIO
import subprocess
import urllib

import error


class Error(error.Error):
    """Raised when stub requirements are not met."""


class CommandError(Error):
    """Raised when StubSubprocess hits a test error."""


class StubTime(object):
    """Replacement for time.* as needed"""

    def __init__(self):
        self.current_time = 0

    def time(self):
        return self.current_time


class StubShutil(object):
    """Stubs for the shutil module."""

    def __init__(self, stub_os):
        self.os = stub_os
        self.Error = Exception
        self.should_copy = []
        self.should_move = []

    def rmtree(self, directory, **_kwargs):
        self.os.path.should_exist = [path for path in self.os.path.should_exist
                                     if not path.startswith(directory)]
        self.os.path.should_be_dir = [path
                                      for path in self.os.path.should_be_dir
                                      if not path.startswith(directory)]

    def copytree(self, start, end):
        self.os.path.should_exist += [path.replace(start, end, 1)
                                      for path in self.os.path.should_exist
                                      if path.startswith(start)]
        self.os.path.should_be_dir += [path.replace(start, end, 1)
                                       for path in self.os.path.should_be_dir
                                       if path.startswith(start)]

    def copy2(self, start, end):
        normalized_end = end.rstrip('/')
        # Ensure the source exists.
        if start not in self.os.path.should_exist:
            raise IOError('Cannot copy non-existent {0} to {1}'.format(start,
                                                                       end))

        if (start, normalized_end) in self.should_copy:
            self.should_copy.remove((start, normalized_end))
            # Note, the containing destination path is not checked.
            self.os.path.should_exist += [normalized_end]
        else:
            raise IOError('Not supposed to copy {0} to {1}'.format(start, end))

    def copystat(self, start, end):
        pass

    def move(self, start, end):
        if not (start, end) in self.should_move:
            raise IOError('Not supposed to move {0} to {1}'.format(start, end))
        self.should_move.remove((start, end))

    def copyfileobj(self, src, dst):
        dst.contents += src


class StubGlob(object):
    """Replacement for glob.* as needed"""

    def __init__(self, stub_os):
        self.os = stub_os

    def iglob(self, pattern):
        # Stub supports only a trailing * as a pattern.
        if pattern.count('*') == 1 and pattern[-1] == '*':
            matches = []
            for path in self.os.path.should_exist:
                if not path.startswith(pattern[:-1]):
                    continue
                # glob doesn't match subpaths.
                if '/' in path[len(pattern[:-1]):]:
                    continue
                matches.append(path)
            return matches

        raise OSError('Unrecognized glob pattern {0}'.format(pattern))

    def glob(self, pattern):
        return self.iglob(pattern)


class StubOs(object):
    """Stubs for the os module."""

    def __init__(self):
        self.cwd = '/'
        self.environ = {}
        self.pathsep = ':'
        self.should_makedirs = []
        self.should_stat = {}
        self.system_commands = []
        self.system_returns = []
        self.should_chmod = []
        self.should_create_link = []
        self.path = self.StubPath(self)
        self._uname = ('', '', '', '', '')

    def uname(self):
        return self._uname

    def getcwd(self):
        return self.cwd

    def SetUname(self, value):
        if len(value) != 5:
            raise OSError('Uname must be 5-tuple')
        self._uname = value

    class _StatVal(object):
        """Stub return type for os.stat."""
        def __init__(self, **kwargs):
            keys = ('st_mode', 'st_ino', 'st_dev', 'st_nlink', 'st_uid',
                    'st_gid', 'st_size', 'st_atime', 'st_mtime', 'st_ctime')
            for k in keys:
                self.__dict__[k] = kwargs.get(k, 0)

    def lstat(self, path):
        return self.stat(path)

    def stat(self, path):
        if not self.path.exists(path):
            raise OSError('Can not stat non-existant path "{0}"'.format(path))
        v = {}
        if path in self.should_stat:
            v = self.should_stat[path]
        return self._StatVal(**v)

    def chmod(self, path, mode):
        if not (path, mode) in self.should_chmod:
            raise OSError('Not supposed to chmod ({0}, {1})'.format(path, mode))
        self.should_chmod.remove((path, mode))

    def makedirs(self, path):
        path = path.rstrip('/')
        if path in self.should_makedirs:
            self.should_makedirs.remove(path)
            while path != '' and path != '/':
                self.path.should_be_dir.append(path)
                self.path.should_exist.append(path)
                path = os.path.dirname(path)
            return True
        raise OSError('Computer says no to makedirs {0}'.format(path))

    def symlink(self, source, link):
        if self.path.exists(link):
            raise OSError('Link name already exists')
        if not (source, link) in self.should_create_link:
            raise OSError('Not supposed to create link {} -> {}'.format(link,
                                                                        source))
        self.path.should_link[link] = source
        self.path.should_exist.append(link)

    def readlink(self, path):
        if path in self.path.should_link:
            return self.path.should_link[path]
        raise OSError(
            'Bad argument to readlink - missing or non-link: {}'.format(path))

    def remove(self, f):
        while f in self.path.should_exist:
            self.path.should_exist.remove(f)
        while f in self.path.should_link:
            del self.path.should_link[f]

    def rmdir(self, f):
        for m in self.path.should_exist:
            if m.startswith(f) and m != f:
                raise OSError('Directory not empty: {}'.format(f))
        while f in self.path.should_exist:
            self.path.should_exist.remove(f)
        while f in self.path.should_link:
            del self.path.should_link[f]

    def walk(self, root, _topdown=False):
        # Note: this does not match the specs of os.walk, but is
        # close enough for our purposes.
        return [(p[:p.rfind('/')], None, [p.split('/')[-1]])
                for p in self.path.should_exist
                if p.startswith(root) and not p in self.path.should_be_dir]

    def listdir(self, root):
        if not self.path.isdir(root):
            raise IOError('{0} is not a dir'.format(root))
        return [self.path.basename(p) for p in self.path.should_exist
                if self.path.dirname(p) == root]

    def system(self, cmdline):
        ret = self.system_returns.pop(0)
        self.system_commands.append(cmdline)
        return ret

    def utime(self, path, _times):
        if path not in self.path.should_exist:
            raise IOError('No such file or directory: {0}'.format(path))

    class StubPath(object):
        """Stubs for the os.path module."""
        sep = os.path.sep

        def __init__(self, stub_os):
            self.should_exist = []
            self.should_be_dir = []
            self.should_link = {}
            self._os = stub_os

        def exists(self, path):
            return path in self.should_exist

        def islink(self, path):
            return path in self.should_link

        def isfile(self, path):
            return path in self.should_exist and not path in self.should_be_dir

        def samefile(self, path1, path2):
            """Subject to infinite loops with cyclical links."""
            while path1 in self.should_link:
                path1 = self.should_link[path1]
            while path2 in self.should_link:
                path2 = self.should_link[path2]
            if not path1 in self.should_exist:
                raise OSError("{0} does not exist".format(path1))
            elif not path2 in self.should_exist:
                raise OSError("{0} does not exist".format(path2))
            else:
                return path1 == path2

        def join(self, *components):
            return os.path.join(*components)

        def dirname(self, f):
            return os.path.dirname(f)

        def basename(self, f):
            return os.path.basename(f)

        def isdir(self, f):
            return f.rstrip('/') in self.should_be_dir

        def relpath(self, sub, root):
            # For stub purposes, assume they are actually subpaths; no ..
            return sub[(len(root) + 1):]  # + 1 to cut out the '/'.

        def abspath(self, p):
            return os.path.abspath(p)

        def expanduser(self, path):
            user = self._os.environ['HOME']
            return path.replace('~', user)


class StubFile(object):
    """Stubs for a file object."""

    def __init__(self, contents=''):
        self.contents = string.split(contents, '\n')
        self.line = 0

    def __iter__(self):
        return self

    def next(self):
        """Return the next line."""
        result = self.readline()
        if not result:
            raise StopIteration
        return result

    def readline(self):
        """Return the next line."""
        if self.line >= len(self.contents):
            return ''

        suffix = '\n'
        if len(self.contents) -1 == self.line:
            suffix = ''
        result = self.contents[self.line] + suffix
        self.line += 1
        return result

    def read(self):
        return string.join(self.contents, '\n')

    def write(self, contents):
        """Currently clobbers all contents and doesn't fix iters."""
        self.contents = string.split(contents, '\n')


class StubHashlib(object):
    """Stubs for the hashlib module."""

    def __init__(self):
        self.should_return = None

    class StubHash(object):
        def __init__(self, should_return):
            self.should_return = should_return

        def hexdigest(self):
            return self.should_return

    def sha256(self, _):
        return self.StubHash(self.should_return)


class StubOpen(object):
    """Stubs the open() function."""

    def __init__(self, stub_os):
        self.os = stub_os
        self.files = {}

    def open(self, path, *args):
        return self.StubOpenFile(path, self, *args)

    class StubOpenFile(object):
        """Stubs the file object returned from open()."""

        def __init__(self, path, parent, *args):
            self.parent = parent
            self.path = path
            if 'w' in args:
                self.parent.os.path.should_exist.append(self.path)
                if not self.path in self.parent.files:
                    self.parent.files[self.path] = StubFile()

        def _file(self):
            if (self.parent.os.path.exists(self.path) and
                    self.parent.files.has_key(self.path)):
                return self.parent.files[self.path]
            raise IOError('File {0} does not exist'.format(self.path))

        def __enter__(self):
            result = self._file()
            result.line = 0
            return result

        def __exit__(self, exc_type, value, traceback):
            # Nothing to do here, if an exception was raised the caller will
            # re-raise.
            return


def _IsSubset(subset, master_set):
    """Returns True if subset <= master_set or subset is None.

    Works for lists or dicts, but does not recurse into dicts, e.g.:
        _IsSubset({a: 1}, {a: 1, b: 2}) --> True
        _IsSubset({x: {a: 1}}, {x: {a: 1, b: 2}}) --> False
    """
    if subset is None:
        return True

    try:
        return all(v == master_set[k] for k, v in subset.items())
    except KeyError:
        # subset had a key that was not present in master_set.
        return False
    except AttributeError:
        # subset has no items() function, is not a dictionary.
        return all(v in master_set for v in subset)


class StubSubprocess(object):
    """Stubs for the subprocess module.

    General usage is based off how the Python mock module works:
        1. Create the expected command with the desired outputs.
        2. Code under test makes the subprocess calls.
        3. Check the call args (optional).

    Examples:
        stub_subprocess = StubSubprocess()

        # Create the expected command, make the call, then check the args.
        subproc = stub_subprocess.AddCommand(stdout='foo')
        CodeUnderTestCallsEchoFoo()
        subproc.AssertCallWas(['echo', 'foo'])

        # You can also just check a subset of args.
        subproc = stub_subprocess.AddCommand(stdout='bar')
        CodeUnderTestCallsEchoBarToPipe()
        subproc.AssertCallContained(['bar'], stdout=stub_subprocess.PIPE)

        # The expected args may also be set at command creation rather than
        # checked afterwards for convenience.
        stub_subprocess.AddCommand(['echo', 'foo'])
        CodeUnderTestCallsEchoBar()  # Will raise CommandError.

    In addition there is AddFileCommand() that contains some additional
    functionality to look for and create files in a StubOs object.
    """

    PIPE = subprocess.PIPE
    STDOUT = subprocess.STDOUT

    def __init__(self, stub_os=None, stub_open=None):
        """Creates a StubSubprocess object.

        stub_open and stub_os are only required if the test is using
        NewFileCommand(), since this needs to interact with a file system.

        Args:
            stub_os: a StubOs object for FileCommand to use.
            stub_open: a StubOpen object for FileCommand to use.
        """
        # Holds pending Commands to pass to the code under test.
        self._commands = []

        # Common references used by created Commands.
        self.stub_os = stub_os
        self.stub_open = stub_open

    def AddCommand(self, *args, **kwargs):
        """Creates a new Command and adds to our expected queue."""
        command = self._Command(*args, **kwargs)
        command.parent = self
        self._commands.append(command)
        return command

    def AddFileCommand(self, *args, **kwargs):
        """Creates a new FileCommand and adds to our expected queue.

        FileCommand objects require that self.stub_os and self.stub_open
        have been set before the subprocess is executed.
        """
        command = self._FileCommand(*args, **kwargs)
        command.parent = self
        self._commands.append(command)
        return command

    def Popen(self, args, **kwargs):
        """Stubs subprocess.Popen().

        Args:
            args: subprocess command args.
            kwargs: subprocess configuration args.

        Returns:
            A Popen-like object with the necessary function stubs.

        Raises:
            CommandError: No subprocess was expected.
        """
        if not self._commands:
            raise CommandError('Unexpected subprocess {}'.format(args))

        # Set the actual call args, and make sure any expectations that were
        # set on the args have been met.
        command = self._commands.pop(0)
        command.SetCallArgs(args, **kwargs)
        return command

    def call(self, args, **kwargs):
        """Stubs subprocess.call(). Returns the exit code."""
        popen = self.Popen(args, **kwargs)
        popen.communicate()
        return popen.returncode

    class _Command(object):
        """Implementation for mocking out a single subprocess command.

        This is private to prevent tests from trying to create these objects
        directly; stub_subprocess.AddCommand() must be used instead to
        properly set up subprocess tracking and variables.

        This class also stubs out the necessary functions to replace a Popen
        object in the code under test.
        """

        def __init__(self, args=None, ret_out='', ret_err='', ret_code=0,
                     side_effect=None, init_side_effect=None):
            """Initializes the expected behavior.

            Args:
                args: a subset of the expected command args to be called with,
                    or None to allow any command.
                ret_out: string to return as subprocess stdout.
                ret_err: string to return as subprocess stderr.
                ret_code: exit code to return from subprocess.
                side_effect: optional function to execute when the subprocess
                    runs.
                init_side_effect: optional function to execute when Popen is
                    called.
            """
            self._ret_out = ret_out
            self._ret_err = ret_err
            self._ret_code = ret_code
            self._side_effect = side_effect
            self._init_side_effect = init_side_effect
            self._expected_args = args

            self.stdout = StringIO.StringIO(ret_out)
            self.stderr = StringIO.StringIO(ret_err)

            # Set by the parent StubSubprocess object after creation.
            self.parent = None

            # args/kwargs are set by SetCallArgs() once they are known.
            self._args = None
            self._kwargs = None

            # These are set by us when the subprocess runs.
            self.returncode = None
            self._ran = False

        def _CheckRan(self):
            if not self._ran:
                raise CommandError('Command {} never ran.'.format(self._args))

        def SetCallArgs(self, args, **kwargs):
            if self._init_side_effect:
                self._init_side_effect()
            self._args = args
            self._kwargs = kwargs
            if not _IsSubset(self._expected_args, args):
                raise CommandError(
                    'Command {} got mismatched args {}'.format(
                        self._expected_args, args))

        def GetCallArgs(self):
            """Returns the call args as an (args, kwargs) tuple."""
            self._CheckRan()
            return self._args, self._kwargs

        def AssertCallWas(self, args, **kwargs):
            """Raises CommandError if the call was not exactly args/kwargs."""
            self._CheckRan()
            if not (self._args == args and self._kwargs == kwargs):
                raise CommandError(
                    'AssertCallWas: expected {} {}, actual {} {}'.format(
                        args, kwargs, self._args, self._kwargs))

        def AssertCallContained(self, args=None, **kwargs):
            """Returns True if the call contained args/kwargs.

            args/kwargs can be a subset of the actual call, but anything
            specified in args/kwargs must match the call exactly.
            """
            self._CheckRan()
            if not (_IsSubset(args, self._args) and _IsSubset(kwargs,
                                                              self._kwargs)):
                raise CommandError(
                    'AssertCallContained: expected {} {}, actual {} {}'.format(
                        args, kwargs, self._args, self._kwargs))

        def communicate(self, _input=None):
            """Stubs Popen.communicate(). Returns (stdout, stderr)."""
            if self._side_effect:
                self._side_effect()

            self._ran = True
            self.returncode = self._ret_code
            out = None
            err = None
            if self._kwargs.get('stdout') == self.parent.PIPE:
                out = self._ret_out
            if self._kwargs.get('stderr') == self.parent.PIPE:
                err = self._ret_err
            return (out, err)

        def wait(self):
            """Stubs Popen.wait(). Returns returncode."""
            self.communicate()
            return self.returncode

    class _FileCommand(_Command):
        """Adds some additional file-checking functionality to _Command.

        Additional features include the ability to verify that required
        input files exist (either by argument position or name) and create
        required output files (either by argument position or name).

        This is useful for testing things like git which need to interact
        with the file system.

        To use _FileCommand, the parent StubSubprocess must have valid
        |stub_os| and |stub_open| members.
        """

        def __init__(self, args=None, in_args=None, out_args=None, pos_in=None,
                     pos_out=None, ret_out='', ret_err='', ret_code=0):
            """Initializes expected input/output file information.

            Args not listed below are the same as the _Command base class.

            Args:
                in_args: list of argument names whose values are input files.
                out_args: list of argument names whose values are output files.
                pos_in: list of argument indices whose values are input files.
                pos_out: list of argument indices whose values are output files.
            """
            # pylint: disable=protected-access
            super(StubSubprocess._FileCommand, self).__init__(
                args=args, ret_out=ret_out, ret_err=ret_err, ret_code=ret_code,
                side_effect=self._CheckFiles)

            self._input_args = in_args or []
            self._output_args = out_args or []
            self._positional_inputs = pos_in or []
            self._positional_outputs = pos_out or []

        def _CheckFiles(self):
            """Verifies that input files exist and creates output files."""
            # Grab a reference to the common stubs held by our parent.
            stub_os = self.parent.stub_os
            stub_open = self.parent.stub_open

            # Split any '-x=y' args to make them nicer for processing.
            split_args = []
            for arg in self._args:
                split_args += arg.split('=')

            # Helper to return the value of argument |name|.
            def GetArgByName(name):
                return split_args[split_args.index(name) + 1]

            # Check that all inputs exist.
            for req in self._input_args:
                req_arg = GetArgByName(req)
                if not stub_os.path.exists(req_arg):
                    raise CommandError(
                        'Required input file {} for command {} is'
                        ' missing'.format(req_arg, self._args))
            for req_index in self._positional_inputs:
                path = self._args[req_index]
                if not stub_os.path.exists(path):
                    raise CommandError(
                        'Required input file {} for command {} is'
                        ' missing'.format(path, self._args))

            # "Create" all outputs.
            for out in self._output_args:
                out_arg = GetArgByName(out)
                stub_os.path.should_exist.append(out_arg)
                stub_open.files[out_arg] = StubFile()
            for out_index in self._positional_outputs:
                out_arg = self._args[out_index]
                stub_os.path.should_exist.append(out_arg)
                stub_open.files[out_arg] = StubFile()


class StubFileinput(object):
    """Stubs for fileinput as necessary."""

    def __init__(self):
        self.should_touch = []

    # pylint: disable=unused-argument
    def input(self, filename, inplace=True):
        """If supposed to input, skip generation. Otherwise error."""
        if filename in self.should_touch:
            return []
        raise OSError('Not supposed to fileinput on file {}'.format(filename))


class StubTempfile(object):
    """Stubs for tempfile as necessary."""

    def __init__(self, os_lib, temp_dir='/tmp/dir'):
        self.os = os_lib
        self.temp_dir = temp_dir

    def mkdtemp(self, **_kwargs):
        self.os.makedirs(self.temp_dir)
        return self.temp_dir


class StubUrllib(object):
    """Stubs for urllib as necessary."""

    def urlencode(self, fields):
        """Url encode the given fields.

        Dictionaries in Python are not ordered, so for unit testing, calls to
        urllib.urlencode(fields) with a dictionary may return the url-encoded
        string with the parameters in an arbitrary order.  So instead, convert
        the |fields| dictionary into a sorted list of tuples, which
        urllib.urlencode(..) will treat as ordered and provide a consistent
        output string.
        """
        sorted_fields = sorted(fields.items())
        return urllib.urlencode(sorted_fields)


class StubUuid(object):
    """Stubs for uuid as necessary."""

    def __init__(self, generate='generated_uuid'):
        self.generate = generate

    def uuid4(self):
        return self.generate


class StubContextManager(object):
    """A generic context manager."""
    def __enter__(self):
        pass

    def __exit__(self, *args):
        pass
