#
# Copyright (C) 2015 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

"""Configures the environment for developing a product."""

import os

from cli import clicommand
from commands.product import constants
from core import config
from core import product
from core import util

class Envsetup(clicommand.Command):
    """Generate the environment for the product in the CWD."""

    @staticmethod
    def Args(parser):
        # TODO(wad)(b/25951098): Set the current version here.
        parser.add_argument('-p', '--product_path',
                            default=util.GetProductDir(),
                            help='Path to the product')
        parser.add_argument('--generate', required=False, default=False,
                            action='store_true',
                            help='If true, outputs the envsetup.sh file')
        # TODO(wad)(b/25951597): Add 'shell' argument to select a non-sh format.

    def Run(self, args):
        if args.product_path is None:
            print constants.MSG_NO_PRODUCT_PATH
            return 1

        # As product_path will end up in the shell environment, we can't
        # rely on a relative path being safe for reinvocation.
        product_path = os.path.abspath(args.product_path)
        store = config.ProductFileStore(product_path)
        env = product.Environment(store, product_path)
        if args.generate is False:
            print env.environment()
        else:
            print 'Generating envsetup.sh . . .'
            env.envsetup()
        return 0
