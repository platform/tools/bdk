#
# Copyright (C) 2016 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

"""Sets up an image spec."""


from __future__ import print_function

import os

from bsp import operating_system
from cli import clicommand
from core import util
from project import platform


IMAGE_SPEC_TEMPLATE = """\
<project>
  <config>
    <!-- Set the default target for subsequent BDK commands. -->
    <default target="{project}-userdebug"/>
  </config>

  <!-- Below is an example pack named "main" in the namespace "{project}".

       A pack defines what files must be copied from your build directories
       to a working image as well as what other runtime dependencies are
       needed.

       This sample pack requires the os core, then copies the contents of
       service/{project}/bin/ to the /system/bin/ folder of the output
       image.  -->
  <packs namespace="{project}">
    <pack name="main">
      <!-- Example rules to require os core and copy binaries to the image.
      <requires pack="os.core"/>
      <copy recurse="true" from="service/{project}/bin/" to="/system/bin/"/>
      -->
    </pack>
  </packs>

  <targets>
    <!-- Two targets are defined below that use the sample pack above to
         determine what files must be placed in the image.  The only difference
         between the two, other than their names, is that one uses a "user"
         build of the OS (no/minimal debugging) and the other uses a "userdebug"
         build which includes debugging information.  -->
    <target name="{project}-release" pack="{project}.main"
            os="{os_name}" os-version="{os_version}"
            board="{board}" board-version="{board_version}"
            build="user"/>
    <target name="{project}-userdebug" pack="{project}.main"
            os="{os_name}" os-version="{os_version}"
            board="{board}" board-version="{board_version}"
            build="userdebug"/>
  </targets>
</project>
"""

class Init(clicommand.Command):
    """Create an image spec in the current directory."""

    @staticmethod
    def Args(parser):
        parser.add_argument('--board', required=True,
                            help='The board model.')
        parser.add_argument('--board_version',
                            default='0.0.0',
                            help='The BSP version to use.')
        parser.add_argument('--os_name', default='brillo',
                            help='The OS name.')
        parser.add_argument('--os_version',
                            default='',
                            help='The OS version.')
        parser.add_argument('--project',
                            default=os.path.basename(os.getcwd()),
                            help='The project name. Default current dir name.')

    def Run(self, args):
        filename = os.path.join(os.getcwd(), util.PROJECT_SPEC_FILENAME)

        # Create a platform object to check if requested OS and BSP
        # are compatible (will raise if not), and then warn if the
        # components need to be downloaded.
        platform_ = platform.Platform(args.os_name, args.os_version,
                                      args.board, args.board_version,
                                      platform.BUILD_TYPE_USERDEBUG)
        try:
            platform_.verify_downloaded()
        except platform.NotDownloadedError as e:
            print('Warning: {}'.format(e))

        sample = IMAGE_SPEC_TEMPLATE.format(
            board=args.board, board_version=args.board_version,
            os_name=args.os_name, os_version=args.os_version,
            project=args.project)

        if os.path.isfile(filename):
            choice = util.GetUserInput('The file {} already exists.  Do you '
                                       'wish to overwrite it?\n'
                                       '(y/N) '.format(
                                           filename)).strip().upper()
            if not choice or choice != 'Y':
                print('Project spec not written.  Exiting.')
                return 1

        try:
            with open(filename, 'w') as f:
                print(sample, file=f)
            print('Starter project spec written to: {}.'.format(filename))
        except IOError as e:
            print('Unable to write to file {}.'.format(filename))
            print('Error: {}'.format(e))
            return 1

        return 0
