#
# Copyright (C) 2016 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

"""CLI adb command."""


from __future__ import print_function

from cli import clicommand
from core import tool


class Adb(clicommand.Command):
    """Calls the adb executable."""

    remainder_arg = ('args', 'Arguments to pass through to adb.')

    @staticmethod
    def Args(parser):
        Adb.AddProductArgs(parser)

    def Run(self, args):
        """Runs adb.

        Args:
            args: parsed argparse namespace.

        Returns:
            0 if successful, an exit code otherwise.
        """
        _, target = self.GetSpecAndTarget(args)
        platform = target.platform

        adb_tool = tool.HostToolWrapper('adb', platform)
        if not adb_tool.exists():
            print('Could not find adb executable at {}. Use `bdk build '
                  'platform` to build it.'.format(adb_tool.path()))
            return 1

        adb_tool.run(args.args)
        return 0
