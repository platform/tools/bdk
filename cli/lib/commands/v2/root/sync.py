#
# Copyright (C) 2016 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

"""CLI command to adb sync the attached device."""


from __future__ import print_function
import os

from cli import clicommand
from core import image_build
from core import tool
from project import dependency


# TODO(dpursell): switch to odm directory once we put user artifacts there
# and adb supports it (http://b/27740246).
_SYNC_DIR = 'system'


class Sync(clicommand.Command):
    """Syncs files to the attached device.

    This provides a fast workflow to iterate on changes, but can only
    download to system/. Any other files must be updated using a full
    `bdk flash`.

    Sync only pushes new files and updates existing files; files on
    the device that no longer exist locally will not be deleted.

    This will root and remount the device in order to enable writing to
    the file system. Does not reboot the device or restart any processes
    after syncing.

    If only one device is attached, it will be targeted by default. If
    more than one device is attached, a specific target can be passed
    with -s or by setting the ANDROID_SERIAL environment variable.
    """

    @staticmethod
    def Args(parser):
        Sync.AddProductArgs(parser)
        parser.add_argument('-s', metavar='<specific device>', default=None,
                            help='Target a specific device.')
        parser.add_argument('-l', '--list_only', action='store_true',
                            help='List only; do not make any changes.')

    def Run(self, args):
        """Runs the sync procedure.

        Args:
            args: parsed argparse namespace.

        Returns:
            0 if successful, an exit code otherwise.
        """
        spec, target = self.GetSpecAndTarget(args)
        platform = target.platform
        platform.verify_downloaded()

        image_build.AddPlatformOsPacks(spec, platform)

        mountpoint = os.path.join(os.path.sep, _SYNC_DIR)
        try:
            image_build.CreateTargetCache(
                spec, target, mountpoint=mountpoint, update=True)
        except dependency.Error as e:
            print('Dependencies unmet for target {}: {}'.format(target.name, e))
            return 1

        # TODO(dpursell): we might want to start encapsulating adb behind
        # a Python class instead of calling it directly (http://b/27854198).
        adb_tool = tool.HostToolWrapper('adb', platform)
        if not adb_tool.exists():
            print('Could not find adb executable at {}.  Use '
                  '`bdk build platform` to build it.'.format(adb_tool.path()))
            return 1

        device_args = ['-s', args.s] if args.s else []

        # The device must be rooted and remounted before we can sync to it. We
        # could try to check for these conditions first and skip the commands if
        # we're in the right state already, but these are very fast if a device
        # is already rooted and remounted so it's probably not worth it.
        if not args.list_only:
            for command in ('root', 'remount'):
                adb_tool.run(device_args + [command])

        # Point adb to the user artifact cache for syncing. This means changes
        # to the platform won't be pushed by `bdk sync`, which is good because
        # platform changes should probably require a full image flash.
        adb_tool.environment['ANDROID_PRODUCT_OUT'] = (
            spec.config.artifact_cache_for_target(target))

        # adb does it's own parsing rather than using a library, so we have to
        # be pretty careful to specify args the way it expects. Usage must be:
        #     adb [-s serial] sync [-l] dir
        sync_args = (['-l'] if args.list_only else []) + [_SYNC_DIR]
        adb_tool.run(device_args + ['sync'] + sync_args)
        return 0
