#
# Copyright (C) 2016 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#


"""Builds a system image."""


import os

from cli import clicommand
from core import image_build
from project import dependency


class Image(clicommand.Command):
    """Build a image for a product."""

    @staticmethod
    def Args(parser):
        Image.AddProductArgs(parser)
        parser.add_argument('--type', choices=image_build.IMAGE_TYPES,
                            default=image_build.IMAGE_TYPE_ODM, required=False,
                            help='Specifies which image type to build.'
                                 '(default: {})'.format(
                                     image_build.IMAGE_TYPE_ODM))

    def Run(self, args):
        spec, target = self.GetSpecAndTarget(args)
        platform = target.platform
        # Needs to be downloaded since we use some of the prebuilt tools.
        platform.verify_downloaded()

        image_build.AddPlatformOsPacks(spec, platform)

        try:
            mountpoint = os.path.join(os.path.sep, args.type)
            image_build.CreateTargetCache(spec, target, mountpoint, update=True)
        except dependency.Error as e:
            print 'Dependencies unmet for target {}: {}'.format(target.name, e)
            return 1

        try:
            ret = image_build.BuildImage(args.type, target, spec.config)
        except (image_build.Error, IOError) as e:
            print 'Error building image for target {} at {}: {}'.format(
                target.name, target.origin, e)
            return 1

        if ret:
            print 'Failed to build image (unknown error code {})'.format(ret)
        else:
            print 'Successfully built image in {}'.format(
                spec.config.output_dir)
        return ret
