#
# Copyright (C) 2016 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#


"""Builds the Brillo platform."""


from cli import clicommand
from core import build
from selinux import policy


class Platform(clicommand.Command):
    """Build a Brillo platform."""

    remainder_arg = ('args', 'Arguments to pass through to make.')

    @staticmethod
    def Args(parser):
        Platform.AddProductArgs(parser)

    def Run(self, args):
        """Runs the platform build.

        Returns:
            0 if successful, an exit code otherwise.
        """
        _, target = self.GetSpecAndTarget(args)
        platform = target.platform
        platform.verify_downloaded()

        try:
            # Build the platform cache.
            ret = build.BuildPlatform(platform, args.args)

            # Obtain and cache SELinux information.
            # GetBoardComboSepolicyDirs() will either raise an exception or
            # return a valid value, so no need to update |ret|.
            sepolicy_dirs = policy.GetBoardComboSepolicyDirs(platform)
            policy.SaveSepolicyDirsCache(platform.build_cache, sepolicy_dirs)
        except (build.Error, policy.Error) as e:
            print 'Failed to build platform: {}'.format(e)
            return 1

        if ret:
            print 'Failed to build platform: unknown error code {}'.format(ret)
        else:
            print 'Successfully built platform in {}'.format(
                platform.build_cache)
        return ret
