#
# Copyright (C) 2015 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

"""Downloads BSPs."""


import argparse

from bsp import device
from bsp import manifest
from bsp import status
from cli import clicommand
from core import util


class Update(clicommand.Command):
    """Update an existing BSP, or download a new BSP.

    The software that you are downloading is the property of the software owner
    and is distributed by the software owner. Google is providing the download
    service as a convenience.
    """

    @staticmethod
    def Args(parser):
        parser.add_argument('device', help='Device to download BSP for.')
        parser.add_argument('-a', '--accept_licenses', action='store_true',
                            help=argparse.SUPPRESS)
        parser.add_argument('-e', '--extract_only', nargs='*',
                            help='List of tarballs to use directly instead of '
                            'downloading from the remote host '
                            'listed in the manifest.')
        parser.add_argument('-l', '--link', action='store_true',
                            help=argparse.SUPPRESS)

    def Run(self, args):
        bsp_manifest = manifest.Manifest.from_json(args.manifest)

        device_ = bsp_manifest.devices.get(args.device)
        if not device_:
            print 'Unrecognized device name:', args.device
            return 1

        extract_only = {}
        if args.extract_only:
            extract_only = self.IdentifyTarballs(args.extract_only, device_)
            if len(extract_only) != len(args.extract_only):
                print ('Could not identify all provided tarballs '
                       '(successfully identified {0}).').format(
                           extract_only.keys())
                return 1

        os_ = bsp_manifest.operating_systems['brillo']
        link_os = os_ if args.link else ''

        try:
            device_.install(extract_only, args.accept_licenses, link_os,
                            verbose=True)
        except device.PackageLinkError as e:
            # If the only error was linking, we may still have downloaded fine.
            print e

        # Print and return the results.
        (device_status, status_string) = device_.status(os_, verbose=True)
        print status_string
        if device_status <= status.LINKED:
            # LINKED or INSTALLED
            print 'Successfully installed all packages for {}.'.format(
                device_.full_name)
            return 0
        elif device_status == status.UNRECOGNIZED:
            print 'The following paths exist but do not link to BSP packages:'
            for path in device_.unrecognized_paths(os_):
                print ' * ' + path
            print ('If this is not intentional, consider '
                   'removing them and installing again.')
            return 0
        else:
            print 'Failed to install some packages for {}.'.format(
                device_.full_name)
            return 1

    def IdentifyTarballs(self, tarballs, device_):
        """Matches a list of tarballs to their corresponding packages.

        Args:
            tarballs: a list of tarball files.
            device_: the device to match to packages in.

        Returns:
            A dictionary mapping { package_name : tarball }.

        Raises:
            bsp.package.VersionError if there is a problem getting any version
            (such as a file not being found).
        """
        mapping = {}
        for tarball in tarballs:
            package_name = device_.match_tarball(tarball)
            if package_name:
                mapping[package_name] = tarball

        return mapping


class Download(Update):
    """Alias, see "bdk bsp update"."""
    pass


class Install(Update):
    """Alias, see "bdk bsp update"."""
    pass
