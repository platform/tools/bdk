#
# Copyright (C) 2016 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#


"""Views user config values."""


from cli import clicommand
from core import user_config


class Check(clicommand.Command):
    """Check user configuration values."""

    @staticmethod
    def Args(parser):
        parser.add_argument('action', choices=user_config.EXPOSED,
                            nargs='?', default=None,
                            help='The variable to check the value of.')

    def Run(self, args):
        if not args.action:
            for var in user_config.EXPOSED:
                print '{}:'.format(var)
                self._CheckStatus(var, indent=2)
        else:
            self._CheckStatus(args.action)
        return 0

    def _CheckStatus(self, var, indent=0):
        val = getattr(user_config.USER_CONFIG, var)
        print ' ' * indent + '{}'.format(val)
