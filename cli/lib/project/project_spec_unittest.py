#
# Copyright (C) 2016 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

"""Tests for project_spec.py"""

import StringIO
import unittest

from core import util
from project import common
from project import config_stub
from project import project_spec
from project import dependency
from project import packs
from project import platform_stub
from project import target_stub


MINIMAL_BOARD_XML = '''
<packs namespace="edison.1">
  <pack name="edison_core">
    <provides pack="board.core"/>
  </pack>
</packs>
'''


MINIMAL_OS_XML = '''
<packs namespace="brillo.1">
  <pack name="all_of_brillo">
    <provides pack="os.core"/>
    <requires pack="board.core"/>
  </pack>
</packs>
'''


MINIMAL_BDK_XML = '''
<project>
  <packs namespace="sample">
    <pack name="ledflasher">
      <requires pack="os.core"/>
      <copy recurse="true" from="service/ledflasher/bin/*" to="/system/bin/"/>
    </pack>
  </packs>
  <targets>
    <target pack="sample.ledflasher"
            os="brillo" os-version="1" board="edison" board-version="1"/>
  </targets>
</project>
'''


BDK_XML_TWO_TARGETS = '''
<project>
  <packs namespace="sample">
    <pack name="ledflasher">
      <requires pack="os.core"/>
      <copy recurse="true" from="service/ledflasher/bin/*" to="/system/bin/"/>
    </pack>
  </packs>
  <targets>
    <target pack="sample.ledflasher"
            os="brillo" os-version="1" board="edison" board-version="1"/>
    <target name="another_pack" pack="sample.ledflasher"
            os="brillo" os-version="1" board="edison" board-version="1"/>
  </targets>
</project>
'''


class ProjectSpecTest(unittest.TestCase):
    def setUp(self):
        os = StringIO.StringIO(MINIMAL_OS_XML)
        board = StringIO.StringIO(MINIMAL_BOARD_XML)
        self.minimal_board = packs.PacksFactory().new(file=board)
        self.minimal_os = packs.PacksFactory().new(file=os)

        # Stub out the platform module during tests
        # to avoid needing the bsp manifest.
        self.stub_platform = platform_stub.StubPlatformModule()
        project_spec.targets.target.platform = self.stub_platform

    def tearDown(self):
        pass

    def add_minimal_packs(self, project_spec_):
        for ps in (self.minimal_os, self.minimal_board):
            project_spec_.packmap.update(ps)

    def project_spec_from_str(self, xml):
        s = StringIO.StringIO(xml)
        b = project_spec.ProjectSpec.from_xml(s)
        return b


class ParseTest(ProjectSpecTest):
    def test_minimal_ok(self):
        spec = self.project_spec_from_str(MINIMAL_BDK_XML)
        # Only 1 target, should become default target.
        self.assertEqual(spec.config.default_target, 'sample.ledflasher')

    def test_two_targets_ok(self):
        spec = self.project_spec_from_str(BDK_XML_TWO_TARGETS)
        # 2 targets, neither should be the default.
        self.assertFalse(spec.config.default_target)

    def test_minimal_populated(self):
        b = self.project_spec_from_str(MINIMAL_BDK_XML)
        self.add_minimal_packs(b)
        b.packmap.report_missing()

    def test_minimal_no_os(self):
        b = self.project_spec_from_str(MINIMAL_BDK_XML)
        # There will be no global suppliers of "os.core".
        with self.assertRaises(dependency.UndefinedPackError):
            b.packmap.report_missing()

    def test_minimal_invalid(self):
        # If the platform doesn't validate, we should get a load error.
        self.stub_platform.valid = False
        with self.assertRaises(common.LoadError):
            self.project_spec_from_str(MINIMAL_BDK_XML)

    def test_minimal_wrong_os_version(self):
        b = self.project_spec_from_str(MINIMAL_BDK_XML)
        # Create a mismatched os pack version.
        self.minimal_os.namespace = 'brillo.2'
        self.minimal_board.namespace = 'edison.2'
        self.add_minimal_packs(b)
        with self.assertRaises(dependency.UnsatisfiedVirtualPackError):
            b.targets['sample.ledflasher'].create_submap(b.packmap)

    def test_minimal_wrong_board_version(self):
        b = self.project_spec_from_str(MINIMAL_BDK_XML)
        # Create a mismatched os pack version.
        self.minimal_board.namespace = 'edison.2'
        self.add_minimal_packs(b)
        with self.assertRaises(dependency.UnsatisfiedVirtualPackError):
            b.targets['sample.ledflasher'].create_submap(b.packmap)

    def test_minimal_requires_missing_pack(self):
        xml = MINIMAL_BDK_XML.replace('os.core', 'missing.pack')
        b = self.project_spec_from_str(xml)
        # Create a mismatched os pack version.
        self.add_minimal_packs(b)
        with self.assertRaises(dependency.UndefinedPackError):
            b.packmap.report_missing()
        with self.assertRaises(dependency.UndefinedPackError):
            b.targets['sample.ledflasher'].create_submap(b.packmap)

    def test_minimal_requires_overpopulated_os_virtual(self):
        b = self.project_spec_from_str(MINIMAL_BDK_XML)
        self.add_minimal_packs(b)
        # Add an extra pack that provides os.core so that the
        # autoselection for os prefix fails.
        os = StringIO.StringIO(MINIMAL_OS_XML.replace(
            'all_of_brillo', 'the_other_half'))
        os_dupe = packs.PacksFactory().new(file=os)
        b.packmap.update(os_dupe)
        # No global undefinedness errors.
        b.packmap.report_missing()
        with self.assertRaises(dependency.UnsatisfiedVirtualPackError):
            b.targets['sample.ledflasher'].create_submap(b.packmap)

    def test_minimal_requires_unfulfilled_virtual(self):
        b = self.project_spec_from_str(
            MINIMAL_BDK_XML.replace('os.core', 'some.pack'))
        self.add_minimal_packs(b)
        # Create a single implementation for the virtual, but since it will
        # not be autoselected by the os-prefix, an error will be raised that
        # suggests this new pack: project_extras.all_of_brillo.
        os = StringIO.StringIO(MINIMAL_OS_XML.replace(
            'brillo.1', 'project_extras').replace('os.core', 'some.pack'))
        os_dupe = packs.PacksFactory().new(file=os)
        b.packmap.update(os_dupe)
        b.packmap.report_missing()
        with self.assertRaises(dependency.UnsatisfiedVirtualPackError):
            b.targets['sample.ledflasher'].create_submap(b.packmap)

    def test_bdk_complex_no_os(self):
        p = util.GetBDKPath('schema/testcases/pass/bdk_complex.xml')
        b = project_spec.ProjectSpec.from_xml(p)
        with self.assertRaises(dependency.UndefinedPackError) as upe:
            # Take the default target and it should fail.
            dfl = b.targets[b.config.default_target]
            dfl.create_submap(b.packmap)
        for line in str(upe.exception).split('\n'):
            # Expect only undefined os packs.
            self.assertIn('Undefined pack "os.', line)

    def test_bdk_complex_wrong_os(self):
        p = util.GetBDKPath('schema/testcases/pass/bdk_complex.xml')
        b = project_spec.ProjectSpec.from_xml(p)
        os_paths = ['schema/testcases/pass/brillo.12.xml',
                    'schema/testcases/pass/brillo.12.common.xml']
        for p in os_paths:
            p = util.GetBDKPath(p)
            b.packmap.update(packs.PacksFactory().new(path=p))
        with self.assertRaises(dependency.UnsatisfiedVirtualPackError) as uvpe:
            for tgt in b.targets.values():
                # Test all targets we didnt load an os for.
                if (tgt.platform.os.name != 'brillo' or
                    tgt.platform.os.version != '12'):
                    tgt.create_submap(b.packmap)

        for line in str(uvpe.exception).split('\n'):
            # Expect candidates from brillo.12 because the first target is
            # brillo.8 and will be unable to satisfy virtuals from the os.
            self.assertIn('may satisfy the requirement: [\'brillo.12.', line)

    def test_bdk_complex_fulfilled(self):
        p = util.GetBDKPath('schema/testcases/pass/bdk_complex.xml')
        b = project_spec.ProjectSpec.from_xml(p)
        os_paths = ['schema/testcases/pass/brillo.12.xml',
                    'schema/testcases/pass/brillo.12.common.xml',
                    'schema/testcases/pass/brillo.8.xml',
                    'schema/testcases/pass/brillo.8.common.xml']
        for p in os_paths:
            p = util.GetBDKPath(p)
            b.packmap.update(packs.PacksFactory().new(path=p))
        # No undefined packs
        b.packmap.report_missing()
        expected_target_names = ['doorman_ed', 'doorman_db', 'doorman_ab',
                                 'doorman_2_db', 'doorman_2_ed', 'doorman_2_ab',
                                 'doorman_2_cba']
        self.assertEqual(expected_target_names, b.targets.keys())
        expected_doorman_2_cba_packs = [
            'speech.synth_service', '3p.opencv.libopencv', 'products.doorman',
            'soc.hw.vision.libface_offload', 'brillo.12.0.0.audiorec',
            'products.doorman_v2', '3p.cloud.voice_api',
            'products.doorman_v2_hw', 'speech.local_synth',
            'brillo.12.0.0.core_stuff', 'cam.facerecr_v2',
            'mic.cloud_voicerec', 'mic.local_voicerec',
            'brillo.12.0.0.new_super_cam', '3p.sndobj.libsndobj',
            'brillo.12.0.0.peripheral_iod'].sort()
        t = b.targets['doorman_2_cba']
        tgt_map = t.create_submap(b.packmap)
        self.assertEqual(expected_doorman_2_cba_packs,
                         tgt_map.map.keys().sort())
        expected_files = [
            '/system/bin/cloud_voiced', '/lib/libfacerecr-2.0.so',
            '/system/etc/init.d/local_voiced.init', '/lib/facerecrd',
            '/system/etc/init.d/cloud_voiced.init', '/system/bin/',
            '/system/lib/libsndobj-1.2.so', '/data/voice_api.wsdl',
            '/system/lib/libcloudvoice.so', '/data/speech/training/',
            '/system/bin/local_voiced', '/system/lib/libspeech-be.so',
            '/system/etc/init.d/facerecerd.init',
            '/system/lib/libface_offload.so'].sort()
        self.assertEqual(expected_files,
                         tgt_map.copy_destinations.keys().sort())
        # Make sure all other packs are satisfied.
        for tgt in b.targets.values():
            tgt.create_submap(b.packmap)


class ProjectSpecMethodsTest(unittest.TestCase):
    def setUp(self):
        self.spec = project_spec.ProjectSpec()
        self.target_a = target_stub.StubTarget('a')
        self.spec.add_target(self.target_a)
        self.target_b = target_stub.StubTarget('b')
        self.spec.add_target(self.target_b)
        self.spec.config = config_stub.StubConfig(default_target='b')

    def test_get_target(self):
        self.assertEqual(self.spec.get_target('a'), self.target_a)

    def test_get_default_target(self):
        self.assertEqual(self.spec.get_target(), self.target_b)

    def test_get_target_invalid(self):
        with self.assertRaises(KeyError):
            self.spec.get_target('c')

    def test_get_target_no_default(self):
        self.spec.config.default_target = None
        with self.assertRaises(KeyError):
            self.spec.get_target()

    def test_add_target(self):
        target_c = target_stub.StubTarget('c')
        self.spec.add_target(target_c)
        self.assertEqual(self.spec.get_target('c'), target_c)

    def test_add_target_name_conflict(self):
        target_b_2 = target_stub.StubTarget('b')
        with self.assertRaises(common.LoadErrorWithOrigin):
            self.spec.add_target(target_b_2)
