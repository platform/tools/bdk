#
# Copyright (C) 2016 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


"""This file provides all Targets element related parsing."""


from project import collection
from project import common
from project import loader
from project import target


class TargetsFactory(object):
    @staticmethod
    def new(packmap, **kwargs):
        """Creates a new Targets instance from an XML file or element.

        Walks an ElementTree <target> looking for <target> child
        nodes and instantiates Target objects.

        Args:
            packmap: A PackMap containing all necessary Packs for the parsed
                targets.
            **kwargs: Any valid keywords for loader.PathElementLoader.load()

        Returns:
            new Targets() instance

        Raises
            LoadError: LoadError, or a subclass, will be raised if an error
                occurs loading and validating content from the XML.
        """
        ts = Targets()
        l = loader.PathElementLoader('targets', [])
        root = l.load(**kwargs)
        root.limit_attribs(['version', 'path'])
        targets = {}
        for node in root.findall('target'):
            # The pack name is the default target name.
            name = node.get_attrib('pack')
            if 'name' in node.attrib:
                name = node.get_attrib('name')
            if name in targets:
                raise common.LoadErrorWithOrigin(
                    node,
                    ('Target "{}" redefined.    Previously defined here: '
                     '"{}".'.format(name, targets[name].origin)))
            t = target.Target(name)
            t.load(node)
            if t.pack_name not in packmap.map:
                raise common.LoadErrorWithOrigin(
                    t.origin,
                    'Target "{}" specifies undefined pack "{}"'.format(
                        t.name,
                        t.pack_name))
            targets[name] = t

        ts.origins = l.origins
        ts.entries = targets
        return ts


class Targets(collection.Base):
    """Collection of Target objects."""
    def __init__(self):
        super(Targets, self).__init__('targets')

    @property
    def targets(self):
        return self.entries
