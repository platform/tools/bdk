#
# Copyright (C) 2016 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

"""Tests for config.py"""


import os
import StringIO
import unittest


from project import common
from project import xml_parser
from project.config import Config


class ConfigTest(unittest.TestCase):
    def config_from_str(self, s):
        xml = StringIO.StringIO(s)
        tree = xml_parser.parse(xml)
        config = Config.from_element(tree.getroot())
        return config

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_relative_made_absolute(self):
        config = Config()
        self.assertEqual(os.path.join(os.getcwd(), 'bdk.cache'),
                         config.cache_dir)
        origin = common.Origin()
        origin.source_file = os.path.join('some', 'path', 'file.xml')
        config.origin = origin
        self.assertEqual(os.path.join('some', 'path', 'bdk.out'),
                         config.output_dir)
        path = 'proj.cache'
        xml_s = '<config><cache-dir path="{}"/></config>'.format(path)
        config = self.config_from_str(xml_s)
        self.assertEqual(path, config.cache_dir)

    def test_absolute_stays_absolute(self):
        config = Config()
        path = '/foo/bar'
        config.output_dir = path
        self.assertEqual(path, config.output_dir)
        xml_s = '<config><output-dir path="{}"/></config>'.format(path)
        config = self.config_from_str(xml_s)
        self.assertEqual(path, config.output_dir)
        xml_s = '<config><cache-dir path="{}"/></config>'.format(path)
        config = self.config_from_str(xml_s)
        self.assertEqual(path, config.cache_dir)

    def test_default_cache(self):
        config = Config(base_path=os.sep)
        self.assertEqual(os.path.join(os.sep, 'bdk.cache'), config.cache_dir)
        xml_s = '<config><output-dir path="/some/path"/></config>'
        config = self.config_from_str(xml_s)
        # No absolutizing happens when the source_file is "<StringIO...>"
        # TODO(wad) should we default this (in Origin) to getcwd()?
        self.assertEqual('bdk.cache', config.cache_dir)

    def test_default_output(self):
        config = Config(base_path=os.sep)
        self.assertEqual(os.path.join(os.sep, 'bdk.out'), config.output_dir)
        xml_s = '<config></config>'
        config = self.config_from_str(xml_s)
        # No absolutizing happens when the source_file is "<StringIO...>"
        self.assertEqual('bdk.out', config.output_dir)

    def test_default_target_missing(self):
        config = Config()
        self.assertEqual('', config.default_target)
        xml_s = '<config/>'
        config = self.config_from_str(xml_s)
        self.assertEqual('', config.default_target)

    def test_default_target_supplied(self):
        config = Config()
        config.default_target = 'foo'
        self.assertEqual('foo', config.default_target)
        xml_s = '<config><default target="tgt"/></config>'
        config = self.config_from_str(xml_s)
        self.assertEqual('tgt', config.default_target)

    def test_config_with_attrs(self):
        xml_s = '<config some="pig"/>'
        with self.assertRaises(common.UnknownAttributes):
            self.config_from_str(xml_s)

    def test_config_with_no_children(self):
        xml_s = '<config/>'
        self.config_from_str(xml_s)

    def test_config_with_unknown_child(self):
        xml_s = '<config><random/></config>'
        # TODO(wad): currently, extra nodes are _not_ checked.
        self.config_from_str(xml_s)

    def test_output_dir_with_other_attr(self):
        xml_s = '<config><output-dir path="./" other="bar"/></config>'
        with self.assertRaises(common.UnknownAttributes):
            self.config_from_str(xml_s)

    def test_cache_dir_with_other_attr(self):
        xml_s = '<config><cache-dir path="./" other="bar"/></config>'
        with self.assertRaises(common.UnknownAttributes):
            self.config_from_str(xml_s)

    def test_default_with_other_attr(self):
        xml_s = '<config><default target="xyz" other="bar"/></config>'
        with self.assertRaises(common.UnknownAttributes):
            self.config_from_str(xml_s)
