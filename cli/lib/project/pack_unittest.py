#
# Copyright (C) 2016 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

"""Tests for pack.py"""


import os
import StringIO
import unittest


from project import common
from project import xml_parser
from project.pack import Pack


# TODO(wad): b/27549362
# - Copy to/from attribute content testing
# - Pack dependency and naming enforcement
# - Test for unknown child nodes (not enforced yet).


class PackTest(unittest.TestCase):
    def pack_from_str(self, ns, name, s):
        pack_xml = StringIO.StringIO(s)
        tree = xml_parser.parse(pack_xml)
        pack = Pack(ns, name)
        pack.load(tree.getroot())
        return pack

    def setUp(self):
        self.ns = 'old.name.space'
        self.p = Pack(self.ns, 'my_pack')

    def tearDown(self):
        pass


class AliasTest(PackTest):
    def test_alias_empty(self):
        new_ns = 'new.name.space'
        self.assertNotEqual(self.p.namespace, new_ns)
        self.p.alias(new_ns)
        self.assertEqual(self.p.namespace, new_ns)

    def test_alias_requires_matches(self):
        required = '{}.foo1'.format(self.ns)
        new_ns = 'new.name.space'
        expected = ['{}.foo1'.format(new_ns)]
        self.p.requires.append(required)
        self.assertEqual(self.p.requires, [required])
        self.p.alias(new_ns)
        self.assertEqual(self.p.requires, expected)


TEST_DEFAULT_COPY_XML = """
    <pack name="a_pack">
      <defaults>
        <copy to="/a/location/">
            <set-acl user="some_user"/>
        </copy>
      </defaults>
      <copy from="/a/place"/>
      <copy from="/b/place" to="/c/place">
        <set-acl user="dont_overwrite_me" group="a_group"/>
      </copy>
      <copy from="/d/place">
        <set-acl group="merge"/>
      </copy>
    </pack>
  """

class AttributeExistenceTest(PackTest):
    def test_pack_valid_full(self):
        xml = '<pack name="foo"><defaults/></pack>'
        pack = self.pack_from_str('a', 'b', xml)
        self.assertIsInstance(pack, Pack)

    def test_pack_valid_named_null(self):
        xml = '<pack name="foo"/>'
        pack = self.pack_from_str('a', 'b', xml)
        self.assertIsInstance(pack, Pack)

    def test_pack_invalid_no_name(self):
        xml = '<pack/>'
        with self.assertRaises(common.MissingAttribute):
            self.pack_from_str('a', 'b', xml)

    def test_pack_invalid_extra_attr(self):
        xml = '<pack name="blah" secret="x12"/>'
        with self.assertRaises(common.UnknownAttributes):
            self.pack_from_str('a', 'b', xml)

    def test_defaults_valid_no_attrs(self):
        xml = '<pack name="test"><defaults/></pack>'
        pack = self.pack_from_str('a', 'b', xml)
        self.assertIsInstance(pack, Pack)

    def test_defaults_invalid_extra(self):
        xml = '<pack name="test"><defaults something="is_here"/></pack>'
        with self.assertRaises(common.UnknownAttributes):
            self.pack_from_str('a', 'b', xml)

    def test_defaults_copy_valid_no_attrs(self):
        xml = '<pack name="test"><defaults><copy/></defaults></pack>'
        pack = self.pack_from_str('a', 'b', xml)
        self.assertIsInstance(pack, Pack)

    def test_defaults_copy_valid_all_attrs(self):
        xml = '<pack name="test"><defaults><copy to="/me/"/></defaults></pack>'
        pack = self.pack_from_str('a', 'b', xml)
        self.assertIsInstance(pack, Pack)

    def test_defaults_copy_invalid_extra(self):
        xml = ('<pack name="test"><defaults><copy to="/me/" from="/baz"/>'
               '</defaults></pack>')
        with self.assertRaises(common.UnknownAttributes):
            self.pack_from_str('a', 'b', xml)

    def test_defaults_copy_acl_valid_no_attrs(self):
        xml = ('<pack name="test"><defaults><copy>'
               '<set-acl/></copy></defaults></pack>')
        pack = self.pack_from_str('a', 'b', xml)
        self.assertIsInstance(pack, Pack)

    def test_defaults_copy_acl_valid_all_attrs(self):
        xml = ('<pack name="test"><defaults><copy>'
               '<set-acl perms="0600" fcaps="" '
               'user="a_user" group="a_group" '
               'selabel="test_t"/></copy></defaults></pack>')
        pack = self.pack_from_str('a', 'b', xml)
        self.assertIsInstance(pack, Pack)

    def test_defaults_copy_acl_invalid_extra(self):
        xml = ('<pack name="test"><defaults><copy>'
               '<set-acl perms="0600" fcaps="CAP_SYS_ADMIN CAP_CHOWN" '
               'user="a_user" group="a_group" '
               'selabel="test_t" apparmor_p="gfoo"/></copy></defaults></pack>')
        with self.assertRaises(common.UnknownAttributes):
            self.pack_from_str('a', 'b', xml)

    def test_copy_invalid_no_attrs(self):
        xml = '<pack name="test"><copy/></pack>'
        with self.assertRaises(common.MissingAttribute):
            self.pack_from_str('a', 'b', xml)

    def test_copy_invalid_extra(self):
        xml = '<pack name="test"><copy to="/hi/" from="/bar" baz="123"/></pack>'
        with self.assertRaises(common.UnknownAttributes):
            self.pack_from_str('a', 'b', xml)

    def test_copy_valid_minimal(self):
        xml = '<pack name="test"><copy to="/x" from="/y"/></pack>'
        pack = self.pack_from_str('a', 'b', xml)
        self.assertIsInstance(pack, Pack)

    def test_copy_valid_all(self):
        xml = ('<pack name="test"><copy to="/x/" from="/y/*" recurse="true"/>'
               '</pack>')
        pack = self.pack_from_str('a', 'b', xml)
        self.assertIsInstance(pack, Pack)

    def test_config_invalid_no_attrs(self):
        xml = '<pack name="test"><config/></pack>'
        with self.assertRaises(common.MissingAttribute):
            self.pack_from_str('a', 'b', xml)

    def test_config_invalid_path_only(self):
        xml = '<pack name="test"><config path="/foo"/></pack>'
        with self.assertRaises(common.MissingAttribute):
            self.pack_from_str('a', 'b', xml)

    def test_config_invalid_type_only(self):
        xml = '<pack name="test"><config type="sepolicy"/></pack>'
        with self.assertRaises(common.MissingAttribute):
            self.pack_from_str('a', 'b', xml)

    def test_config_invalid_extra(self):
        xml = ('<pack name="test">'
               '<config type="kernel-fragment" path="/foo" z="1"/></pack>')
        with self.assertRaises(common.UnknownAttributes):
            self.pack_from_str('a', 'b', xml)

    def test_config_valid_all(self):
        xml = ('<pack name="test"><config path="/foo" type="kernel-fragment"/>'
               '</pack>')
        pack = self.pack_from_str('a', 'b', xml)
        self.assertIsInstance(pack, Pack)

    def test_provides_invalid_bare(self):
        xml = '<pack name="test"><provides/></pack>'
        with self.assertRaises(common.MissingAttribute):
            self.pack_from_str('a', 'b', xml)

    def test_provides_invalid_extra(self):
        xml = ('<pack name="test">'
               '<provides pack="hello.bar" from="somewhere"/></pack>')
        with self.assertRaises(common.UnknownAttributes):
            self.pack_from_str('a', 'b', xml)

    def test_provides_valid(self):
        xml = ('<pack name="test">'
               '<provides pack="hello.bar"/></pack>')
        pack = self.pack_from_str('a', 'b', xml)
        self.assertIsInstance(pack, Pack)

    def test_requires_invalid_bare(self):
        xml = '<pack name="test"><requires/></pack>'
        with self.assertRaises(common.MissingAttribute):
            self.pack_from_str('a', 'b', xml)

    def test_requires_invalid_extra(self):
        xml = ('<pack name="test">'
               '<requires pack="hello.bar" from="somewhere"/></pack>')
        with self.assertRaises(common.UnknownAttributes):
            self.pack_from_str('a', 'b', xml)

    def test_requires_valid(self):
        xml = ('<pack name="test">'
               '<requires pack="hello.bar"/></pack>')
        pack = self.pack_from_str('a', 'b', xml)
        self.assertIsInstance(pack, Pack)


class ConfigTest(PackTest):
    # TODO(wad) Replace all '/' with os.path.sep.
    def test_valid_types(self):
        for t in ('sepolicy', 'kernel-fragment'):
            xml = ('<pack name="test"><config path="/foo" type="{}"/>'
                   '</pack>'.format(t))
            pack = self.pack_from_str('a', 'b', xml)
            self.assertIsInstance(pack, Pack)

    def test_invalid_types(self):
        for t in ('a_type', 'firmware-fragment'):
            xml = ('<pack name="test"><config path="/foo" type="{}"/>'
                   '</pack>'.format(t))
            with self.assertRaises(common.LoadErrorWithOrigin):
                self.pack_from_str('a', 'b', xml)

    def test_trailing_slash(self):
        xml = ('<pack name="test"><config path="/foo/" type="kernel-fragment"/>'
               '</pack>')
        with self.assertRaises(common.LoadErrorWithOrigin):
            self.pack_from_str('a', 'b', xml)

    def test_absolute_path(self):
        xml = ('<pack name="test"><config path="/foo" type="kernel-fragment"/>'
               '</pack>')
        pack = self.pack_from_str('a', 'b', xml)
        self.assertIsInstance(pack, Pack)
        self.assertEqual(pack.configs[0].path, '/foo')

    def test_relative_path(self):
        xml = ('<pack name="test"><config path="foo.kconfig" '
               'type="kernel-fragment"/></pack>')
        pack = self.pack_from_str('a', 'b', xml)
        self.assertIsInstance(pack, Pack)
        self.assertEqual(pack.configs[0].path,
                         os.path.join(os.getcwd(), 'foo.kconfig'))


class DefaultCopyPropagationTest(PackTest):
    def setUp(self):
        self._pack = self.pack_from_str('a_namespace', 'a_pack',
                                        TEST_DEFAULT_COPY_XML)

    def test_default_user_merge(self):
        for node in self._pack.copies:
            if node.src == '/d/place':
                self.assertEqual(node.dst, '/a/location/place')
                self.assertEqual(node.acl.group, 'merge')
                self.assertEqual(node.acl.user, 'some_user')

    def test_default_noclobber(self):
        for node in self._pack.copies:
            if node.src == '/b/place':
                self.assertEqual(node.dst, '/c/place')
                self.assertEqual(node.acl.user, 'dont_overwrite_me')
                self.assertEqual(node.acl.group, 'a_group')

    def test_default_to_empty(self):
        for node in self._pack.copies:
            if node.src == '/a/place':
                self.assertEqual(node.dst, '/a/location/place')
                self.assertEqual(node.acl.user, 'some_user')
                self.assertEqual(node.acl.group, node.acl.DEFAULTS['GROUP'])
                self.assertEqual(node.acl.selabel, node.acl.DEFAULTS['SELABEL'])
                self.assertEqual(node.acl.fcaps,
                                 node.acl.DEFAULTS['CAPABILITIES'])
                self.assertEqual(node.acl.perms,
                                 node.acl.DEFAULTS['PERMISSIONS'])
