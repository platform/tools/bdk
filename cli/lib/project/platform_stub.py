#
# Copyright (C) 2016 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


"""Stubs for classes in platform.py."""


import os

from bsp import device_stub
from bsp import operating_system_stub
from test import stubs


class StubPlatform(object):
    """A Stub for the Platform class."""

    def __init__(self, os_name='', os_version='', board='', board_version='',
                 build_type='', os_root='', device_arch='', build_cache='',
                 product_out_cache='', sysroot='', toolchain='', cache_dir='',
                 should_link=False, verify_raises=Exception):
        # Properties.
        self.os = operating_system_stub.StubOperatingSystem(
            name=os_name, root=os_root, version=os_version)
        self.device = device_stub.StubDevice(name=board, version=board_version,
                                             arch=device_arch)
        self.build_type = build_type
        self.build_cache = build_cache
        self.product_out_cache = product_out_cache
        self.sysroot = sysroot
        self.toolchain = toolchain

        # Helpers.
        self.cache_dir = cache_dir
        self.should_link = should_link
        self.verify_raises = verify_raises

    @property
    def os_namespace(self):
        return '{}.{}'.format(self.os.name, self.os.version)

    @property
    def board_namespace(self):
        return '{}.{}'.format(self.device.name, self.device.version)

    def cache_path(self, *relpath):
        return os.path.join(self.cache_dir, *relpath)

    def linked(self):
        if not self.should_link:
            raise StubPlatformModule.Error('Not supposed to link platform.')
        return stubs.StubContextManager()

    def verify_downloaded(self):
        if self.verify_raises:
            # pylint: disable=raising-bad-type
            raise self.verify_raises


class StubPlatformModule(object):

    class Error(Exception):
        pass

    def __init__(self, valid=True):
        self.valid = valid

    def Platform(self, *args, **kwargs):
        if not self.valid:
            raise self.Error
        return StubPlatform(*args, **kwargs)
