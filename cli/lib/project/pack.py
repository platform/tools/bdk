#
# Copyright (C) 2016 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


"""This file provides all Pack element related parsing."""

import os

from project import acl
from project import common


WILDCARD = '*'


class Pack(object):
    """Pack objects represent the <pack> element."""
    def __init__(self, namespace, name):
        self._namespace = namespace
        self._name = name
        self._depends = {'requires':[], 'provides':[]}
        self._copies = []
        self._configs = []
        self._defaults = DefaultCopy()
        self._origin = common.Origin()

    def _alias_puidlist(self, old_ns, new_ns, puidlist):
        """Takes a list of pack ids and changes their prefix in a new list."""
        new_list = []
        for puid in puidlist:
            if puid.startswith('{}.'.format(old_ns)):
                new_list += ['{}{}'.format(new_ns, puid[len(old_ns):])]
            else:
                new_list += [puid]
        return new_list

    def alias(self, new_ns):
        """Swap out the namespace prefix in all absolute ids."""
        self._depends['requires'] = self._alias_puidlist(
            self._namespace, new_ns, self._depends['requires'])
        self._depends['provides'] = self._alias_puidlist(
            self._namespace, new_ns, self._depends['provides'])
        self._namespace = new_ns

    @property
    def namespace(self):
        return self._namespace

    @property
    def origin(self):
        """Returns string indicating where the pack was defined."""
        return self._origin

    @property
    def uid(self):
        """Returns the fully qualified unique pack name"""
        return '{}.{}'.format(self._namespace, self._name)

    @property
    def name(self):
        """Returns the namespace-local pack name."""
        return self._name

    @property
    def requires(self):
        """Returns a list of required fully qualified pack unique names."""
        return self._depends['requires']

    @property
    def provides(self):
        """Returns a list of provided fully qualified pack virtual names."""
        return self._depends['provides']

    def add_provides(self, virtual_name):
        # Duplicates are ignored.
        if virtual_name in self._depends['provides']:
            return
        self._depends['provides'].append(virtual_name)

    @property
    def defaults(self):
        """Returns the DefaultCopy node if defined."""
        return self._defaults

    @defaults.setter
    def defaults(self, default_copy):
        self._defaults = default_copy

    @property
    def copies(self):
        """Returns the list of child Copy objects."""
        return self._copies

    def add_copy(self, copy):
        self._copies.append(copy)

    @property
    def configs(self):
        """Returns the list of child Config objects."""
        return self._configs

    def __repr__(self):
        s = '<pack name="{}">{}{}{}{}{}</pack>'.format(
            self._name,
            ''.join(['<provides pack="{}"/>'.format(p)
                     for p in self._depends['provides']]),
            ''.join(['<requires pack="{}"/>'.format(p)
                     for p in self._depends['requires']]),
            ''.join([str(c) for c in self._copies]),
            self._configs or '',
            self._defaults
        )
        return s

    def load(self, ele):
        """Loads the Pack from a XML element node."""
        ele.limit_attribs(['name'])
        # Override any pre-defined name.
        self._name = ele.get_attrib('name')
        self._origin = ele.origin.copy()
        for child in ele.findall('defaults'):
            self._defaults.load(child)
        for pack_op in self._depends:
            for child in ele.findall(pack_op):
                child.limit_attribs(['pack'])
                n = child.get_attrib('pack')
                if '.' not in n:
                    raise common.LoadErrorWithOrigin(
                        child.origin,
                        ('<{}> must supply fully qualified pack names: '
                         '{}'.format(pack_op, n)))
                self._depends[pack_op].append(n)
        for child in ele.findall('copy'):
            c = Copy(self)
            c.load(child)
            self._copies.append(c)
        for child in ele.findall('config'):
            c = Config(self)
            c.load(child)
            self._configs.append(c)


class CopyType(object):
    DIR = 1
    FILE = 2
    GLOB = 3
    @staticmethod
    def get(path):
        if path.endswith(os.sep):
            return CopyType.DIR
        if path.endswith(WILDCARD):
            return CopyType.GLOB
        return CopyType.FILE


class DefaultCopy(object):
    def __init__(self):
        self._origin = common.Origin()
        self._dst = None
        self._acl = acl.FileAcl(None)

    def load(self, ele):
        ele.limit_attribs([])
        copies = ele.findall('copy')
        if len(copies) == 0:
            return
        if len(copies) != 1:
            raise common.LoadErrorWithOrigin(
                ele.origin, 'Only one copy element may be defined in defaults.')
        ele = copies[0]
        ele.limit_attribs(['to'])
        # TODO(wad) Here and elsewhere, ensure attrib only
        #     has keys that are explicitly supported.
        self._origin = ele.origin.copy()
        if 'to' in ele.attrib:
            self._dst = ele.get_attrib('to')
            if CopyType.get(self._dst) != CopyType.DIR:
                raise common.LoadErrorWithOrigin(
                    self._origin,
                    ('default copy destinations must be directories, suffixed '
                     'by a {}'.format(common.pathsep)))
        acls = ele.findall('set-acl')
        if len(acls) > 1:
            raise common.LoadErrorWithOrigin(
                ele.origin, 'Only one set-acl default may be set')
        if len(acls):
            self._acl.load(acls[0])

    @property
    def dst(self):
        return self._dst

    @property
    def acl(self):
        return self._acl

    def __repr__(self):
        return '<defaults><copy to="{}">{}</copy></defaults>'.format(
            self._dst or '', self._acl)


class Copy(object):
    def __init__(self, pack, dst='', dst_type=CopyType.FILE, src='',
                 src_type=CopyType.FILE):
        self._pack = pack
        self._dst = dst
        self._dst_type = dst_type
        # Set the default "to" if the pack has none.
        if pack is not None and pack.defaults is not None:
            if pack.defaults.dst is not None:
                self._dst = pack.defaults.dst
                self._dst_type = CopyType.get(self._dst)
        self._src = src
        self._src_type = src_type
        self._recurse = False
        self._acl = acl.FileAcl(self)
        self._origin = common.Origin()

    @property
    def pack(self):
        return self._pack

    @property
    def origin(self):
        return self._origin

    def load(self, ele):
        """Loads the Copy from a XML element node (copy)."""
        ele.limit_attribs(['to', 'from', 'recurse'])
        # Always get to from the element.
        if 'to' in ele.attrib:
            self._dst = ele.get_attrib('to')
        # Fail if there is no 'to' default or defined 'to'.
        if self._dst == '':
            self._dst = ele.get_attrib('to')

        self._dst_type = CopyType.get(self._dst)
        self._src = ele.get_attrib('from')
        self._src_type = CopyType.get(self._src)
        recurse = (ele.attrib.get('recurse') or 'false').lower()
        self._origin = ele.origin.copy()
        if recurse == 'true':
            self._recurse = True
        elif recurse == 'false':
            self._recurse = False
        else:
            raise common.LoadErrorWithOrigin(
                self._origin, '<copy> recurse element not "true" or "false"')
        self._recurse = recurse == 'true' or False

        acls = ele.findall('set-acl')
        if len(acls) > 1:
            raise common.LoadErrorWithOrigin(
                ele.origin, 'Only one <set-acl> element is allowed per <copy>')
        if len(acls):
            self._acl.load(acls[0])
        self._reconcile_paths()

    def _reconcile_paths(self):
        if not self._dst.startswith(common.pathsep):
            raise common.LoadErrorWithOrigin(
                self._origin,
                ('<copy> destinations must be absolute (start with a {}): '
                 '{}'.format(common.pathsep, self._dst)))
        if self._recurse:
            # Ensure recursive destinations are paths and require a trailing
            # slash. This is pedantic, but it is better to start explicit.
            if not self._dst_type == CopyType.DIR:
                raise common.LoadErrorWithOrigin(
                    self._origin,
                    '<copy> specifies recursion but the destination "{}" '
                    'does not have a trailing path separator'.format(self._dst))
            # Similarly, recursive sources must be labeled as paths using
            # trailing slashes or end with a wildcard (*) allowing a path to be
            # copied or the contents of a path.
            if self._src_type == CopyType.FILE:
                raise common.LoadErrorWithOrigin(
                    self._origin,
                    '<copy> specifies recursion but the source "{}" '
                    'does not have a trailing path separator or '
                    'wildcard'.format(self._src))
            # If we're copying the path, we could translate it into a more
            # specific glob here, but there is no benefit using normal python
            # helpers.

        # For file copies into a path, compute the final path.
        if not self._recurse:
            if (self._dst_type == CopyType.DIR
                    and self._src_type == CopyType.FILE):
                # foo/bar.txt -> /system/bin/ becomes /system/bin/bar.txt
                self._dst = common.path_join(self._dst,
                                             common.basename(self._src))
                self._dst_type = CopyType.FILE
            elif (self._dst_type == CopyType.DIR
                  and self._src_type == CopyType.DIR):
                # foo/bar/ -> /system/blah becomes /system/blah/bar/
                # (GLOBs are used to copy contents to the same name.)
                self._dst = common.path_join(
                    self._dst,
                    common.basename(self._src.rstrip(common.pathsep)))
            elif (self._src_type == CopyType.GLOB
                  and not self._dst_type == CopyType.DIR):
                raise common.LoadErrorWithOrigin(
                    self._origin,
                    '<copy> source "{}" uses a wildcard but the destination '
                    '"{}" does not have a trailing path separator'.format(
                        self._src, self._dst))

        # Absolutize the sources based on the defining file.
        # This also means that sources must use host separators.
        # TODO(wad) Consider allowing a base to be set by Packs on
        #     inclusion of a file.
        self._src = common.path_to_host(self._src)
        if not os.path.isabs(self._src):
            base_path = os.path.dirname(self._origin.source_file)
            self._src = os.path.abspath(os.path.join(base_path, self._src))

        # Note, if we end up creating an image root, then we can also absolutize
        # the destinations (excepting wildcards).

    @property
    def src(self):
        """Returns the path on the caller's client."""
        return self._src

    @src.setter
    def src(self, src):
        self._src = src

    @property
    def dst(self):
        return self._dst

    @dst.setter
    def dst(self, dst):
        self._dst = dst

    @property
    def src_type(self):
        return self._src_type

    @property
    def dst_type(self):
        return self._dst_type

    @src_type.setter
    def src_type(self, t):
        self._src_type = t

    @dst_type.setter
    def dst_type(self, t):
        self._dst_type = t

    @property
    def recurse(self):
        return self._recurse

    @recurse.setter
    def recurse(self, r):
        self._recurse = r

    @property
    def acl(self):
        return self._acl

    @acl.setter
    def acl(self, acl_):
        self._acl = acl_

    def __repr__(self):
        return '<copy to="{}" from="{}" recurse="{}">{}</copy>'.format(
            self._dst, self._src, self._recurse, self._acl)


class ConfigType(object):
    UNKNOWN = 0
    KERNEL_FRAGMENT = 1
    SELINUX_POLICY = 2
    NAMES = {'kernel-fragment': KERNEL_FRAGMENT,
             'sepolicy': SELINUX_POLICY}

    @staticmethod
    def get(type_name):
        if type_name in ConfigType.NAMES:
            return ConfigType.NAMES[type_name]
        return ConfigType.UNKNOWN


class Config(object):
    def __init__(self, pack):
        self._pack = pack
        self._path = ()
        self._type = ConfigType.UNKNOWN
        self._origin = common.Origin()

    @property
    def pack(self):
        return self._pack

    def load(self, ele):
        """Loads the Config from a XML element node (config)."""
        ele.limit_attribs(['path', 'type'])
        self._origin = ele.origin.copy()
        self._path = common.path_to_host(ele.get_attrib('path'))
        if not isinstance(self._path, basestring):
            raise common.LoadErrorWithOrigin(ele.origin,
                                             'Failed to parse "path" attribute')

        # Anchor a relative path to the origin's location.
        if not os.path.isabs(self._path):
            base_path = os.path.dirname(self._origin.source_file)
            self._path = os.path.abspath(os.path.join(base_path, self._path))
        if self._path.endswith(os.sep):
            raise common.LoadErrorWithOrigin(
                self._origin,
                '@path must specify a file and not a directory: {}'.format(
                    self._path))
        self._type = ConfigType.get(ele.get_attrib('type'))
        if self._type == ConfigType.UNKNOWN:
            raise common.LoadErrorWithOrigin(
                self._origin,
                '@type must be one of {}'.format(ConfigType.NAMES.keys()))

    @property
    def path(self):
        return self._path

    def __repr__(self):
        return '<config type="{}" path="{}"'.format(
            self._type, self._path)
