#
# Copyright (C) 2016 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

"""This file provides a base class for a collection of BDK child elements."""


class Base(object):

    def __init__(self, tag):
        self._tag = tag
        self._entries = {}
        self._origins = []

    def __iter__(self):
        """Enable for entries in collection semantics."""
        return self._entries.values().__iter__()

    @property
    def entries(self):
        """Returns a dict of name to objects."""
        return self._entries

    @entries.setter
    def entries(self, entries):
        self._entries = entries

    @property
    def origins(self):
        """Return the list of Origin objects parsed for this Packs."""
        return self._origins

    @origins.setter
    def origins(self, origins):
        self._origins = origins

    def __repr__(self):
        return '<{}>{}</{}>'.format(
            self._tag, ''.join([str(e) for e in self._entries.values()]),
            self._tag)
