#
# Copyright (C) 2016 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#


"""Stubs for the Config class as needed."""


class StubConfig(object):

    def __init__(self, default_target='', origin='test_config_origin',
                 artifact_cache='', metadata_cache='', output_dir=''):
        self.default_target = default_target
        self.origin = origin
        self.output_dir = output_dir
        self._artifact_cache = artifact_cache
        self._metadata_cache = metadata_cache

    def artifact_cache_for_target(self, _target):
        return self._artifact_cache

    def metadata_cache_for_target(self, _target):
        return self._metadata_cache
