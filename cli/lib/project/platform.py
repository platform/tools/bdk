#
# Copyright (C) 2016 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


"""An object representing a Platform (OS + BSP combination)."""


import os

from bsp import manifest
from core import user_config
from core import util
from project import common


BUILD_TYPE_USER = 'user'
BUILD_TYPE_USERDEBUG = 'userdebug'
BUILD_TYPE_ENG = 'eng'
BUILD_TYPES = (BUILD_TYPE_USER, BUILD_TYPE_USERDEBUG, BUILD_TYPE_ENG)


# Note: this should be kept in sync with
#   commands.environment.setup.SAMPLE_MAKEFILE_TEMPLATE
PLATFORM_CACHE_FORMAT = os.path.join('{user_configured_platform_cache}',
                                     '{os_namespace}',
                                     '{board_namespace}')


class Error(common.Error):
    """General Error for targets."""


class OsError(Error):
    """Raised when there is a problem with the specified os/version."""
    description = 'Invalid OS'


class BoardError(Error):
    """Raised when there is a problem with the specified board/version."""
    description = 'Invalid board'


class BuildTypeError(Error):
    """Raised when an invalid build type is specified."""
    description = 'Invalid build type'


class NotDownloadedError(Error):
    """Raised when the specified OS or BSP isn't downloaded."""
    description = 'Missing download'


class _LinkContext(object):
    """A context manager to allow temporary linking of a device to an OS.

    Attributes:
        platform: The platform to link together.
    """

    def __init__(self, platform):
        self.platform = platform

    def __enter__(self):
        success = False
        try:
            self.platform.device.link(self.platform.os)
            success = True
        finally:
            # If something goes wrong, leave unlinked.
            if not success:
                self.cleanup()

    def __exit__(self, type_, value_, traceback_):
        # TODO(b/28028440): Even if you were linked initially, when
        #     exiting the context the links will be removed.
        self.cleanup()

    def cleanup(self):
        self.platform.device.unlink(self.platform.os)


class Platform(object):
    """A (read-only) OS + BSP combination.

    Initialization raises an Error if an invalid OS, board, or build_type
    is requested.
    """

    def __init__(self, os_name='', os_version='', board='', board_version='',
                 build_type=''):
        """Initialize a Platform.

        Args:
          os_name: The name of the OS for this platform. Must be "brillo".
          os_version: The version of the OS for this platform.
          board: The name of the board for this platform.
          board_version: The version of the board for this platform.
          build_type: The type of build for this platform. Must be one of
              the designated platform.BUILD_TYPES.

        Raises:
          OsError: If the OS requested is invalid.
          BoardError: If the board requested is invalid.
          BuildTypeError: If the build type requested is invalid.
        """
        bsp_manifest = manifest.Manifest.from_json()

        # OS.
        os_ = bsp_manifest.operating_systems.get(os_name)
        if not os_:
            raise OsError('unrecognized name "{}" '
                          '(available OSes: {}).'.format(
                              os_name, bsp_manifest.operating_systems.keys()))
        # For now, since we only have one OS version,
        # fill it in as a default if necessary.
        if not os_version:
            os_version = os_.version
        if os_.version != os_version:
            raise OsError('version {} ({} is the only available '
                          'version for {}).'.format(
                              os_version, os_.version, os_name))
        self._os = os_

        # Device.
        device = bsp_manifest.devices.get(board)
        if not device:
            raise BoardError('unrecognized name "{}". Run `bdk bsp list` '
                             'to see available boards.'.format(
                                 board))
        # For now, since we only have one device version,
        # fill it in as a default if necessary.
        if not board_version:
            board_version = device.version
        if device.version != board_version:
            raise BoardError('version {} ({} is the only available '
                             'version for {}).'.format(
                                 board_version, device.version, board))
        self._device = device

        # TODO(b/27654613): Check OS and board compatibility.

        # Build type.
        if build_type not in BUILD_TYPES:
            raise BuildTypeError('{} (acceptable values are {}).'.format(
                build_type, BUILD_TYPES))
        self._build_type = build_type

        # Helper variable.
        self._cache_path = PLATFORM_CACHE_FORMAT.format(
            user_configured_platform_cache=(
                user_config.USER_CONFIG.platform_cache),
            os_namespace=self.os_namespace,
            board_namespace=self.board_namespace)

    @property
    def os(self):
        return self._os

    @property
    def os_namespace(self):
        return '{}.{}'.format(self.os.name, self.os.version)

    @property
    def board_namespace(self):
        return '{}.{}'.format(self.device.name, self.device.version)

    @property
    def build_type(self):
        return self._build_type

    @property
    def device(self):
        return self._device

    @property
    def build_cache(self):
        return self.cache_path(self.build_type)

    @property
    def product_out_cache(self):
        return os.path.join(self.build_cache, 'target', 'product',
                            self.device.name)

    @property
    def toolchain(self):
        return self.cache_path('toolchain')

    @property
    def sysroot(self):
        return self.cache_path('sysroot')

    def cache_path(self, *relpath):
        return os.path.join(self._cache_path, *relpath)

    def linked(self):
        """A context manager for temporary linkage.

        Note: b/28028440: It is a known bug that after exiting the context,
            the device will be unlinked, whether or not it was linked going in.

        Returns:
            A context manager within which the BSP and OS will be linked.
        """
        return _LinkContext(self)

    def verify_downloaded(self):
        """Checks that a platform is downloaded.

        Raises:
            NotDownloadedError: If the platform isn't downloaded.
        """
        # Check that the OS is downloaded.
        if not self.os.is_available():
            raise NotDownloadedError(
                'OS "{}".'.format(self.os.name))

        # Check that the BSP is downloaded.
        if not self.device.is_available():
            raise NotDownloadedError(
                'Board Support Package for "{0}". '
                'Run `bdk bsp download {0}`.'.format(self.device.name))
