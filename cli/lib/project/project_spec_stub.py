#
# Copyright (C) 2016 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#


"""Stubs for the ProjectSpec class as needed."""


from project import config_stub


class StubProjectSpec(object):

    def __init__(self, targets=None, file_default_target='', file_targets=None,
                 should_read_file=None, packmap=None, config=None):
        self.targets = targets or {}
        self.file_default_target = file_default_target
        self.file_targets = file_targets or {}
        self.should_read_file = should_read_file or []
        self.packmap = packmap
        self.config = config or config_stub.StubConfig()

    def add_packs(self, packs_obj):
        self.packmap.update(packs_obj)

    def from_xml(self, f):
        if not f in self.should_read_file:
            raise IOError('Not supposed to read from file "{}"'.format(f))
        self.targets = self.file_targets
        self.config.default_target = self.file_default_target
        self.should_read_file.remove(f)
        return self


class StubProjectSpecGenerator(object):
    def __init__(self):
        self.targets = {}
        self.file_default_target = ''
        self.file_targets = {}
        self.should_read_file = []

    @property
    def ProjectSpec(self):
        return StubProjectSpec(self.targets, self.file_default_target,
                               self.file_targets, self.should_read_file)
