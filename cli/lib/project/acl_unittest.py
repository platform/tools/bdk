#
# Copyright (C) 2016 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

"""Tests for acl.py"""


import StringIO
import struct
import unittest

from project import common
from project import xml_parser
from project.acl import FileAcl
from project.pack import Copy


class FileAclTest(unittest.TestCase):
    USER = '1000'
    GROUP = '1001'
    FCAPS = '0'
    SELABEL = 'u:object_r:unlabeled:s0'
    PERMS = '0600'
    XML = '<set-acl perms="{}" user="{}" group="{}" fcaps="{}" selabel="{}"/>'

    def setUp(self):
        # Setup a common ACL when needed.
        self.acl = FileAcl(None)
        xml = self.XML.format(
            self.PERMS, self.USER, self.GROUP, self.FCAPS, self.SELABEL)
        node = self.node_from_str(xml)
        self.acl.load(node)

    def tearDown(self):
        pass

    def node_from_str(self, s):
        xml = StringIO.StringIO(s)
        tree = xml_parser.parse(xml)
        return tree.getroot()


    def test_valid_perms(self):
        xml = ('<set-acl perms="0600"/>')
        node = self.node_from_str(xml)
        acl = FileAcl(None)
        acl.load(node)
        self.assertIsInstance(acl, FileAcl)
        self.assertEqual(acl.perms, '0600')

    def test_invalid_perms(self):
        xml = ('<set-acl perms="0609"/>')
        node = self.node_from_str(xml)
        acl = FileAcl(None)
        with self.assertRaises(common.LoadErrorWithOrigin):
            acl.load(node)

    def test_valid_attrs(self):
        self.assertIsInstance(self.acl, FileAcl)
        self.assertEqual(self.acl.perms, self.PERMS)
        self.assertEqual(self.acl.user, self.USER)
        self.assertEqual(self.acl.group, self.GROUP)
        self.assertEqual(self.acl.fcaps, self.FCAPS)
        self.assertEqual(self.acl.selabel, self.SELABEL)

    def test_invalid_attrs(self):
        xml = ('<set-acl perms="0600" user="1000" group="123" fcaps="0"'
               '         custom="value" selabel="u:object_r:unlabeled:s0"/>')
        node = self.node_from_str(xml)
        acl = FileAcl(None)
        with self.assertRaises(common.LoadErrorWithOrigin):
            acl.load(node)

    def test_fs_config_ascii(self):
        FS_CONFIG_FMT = '{} {} {} {} capabilities={}'
        self.acl.copy = Copy(None, dst='/bin/helper')
        entry = self.acl.fs_config(binary=False)
        expected_entry = FS_CONFIG_FMT.format(
            'bin/helper', self.USER, self.GROUP, self.PERMS, self.FCAPS)
        self.assertEqual(entry, expected_entry)

    def test_file_context_ascii(self):
        FS_CONFIG_FMT = '{} {} {} {} capabilities={}'
        self.acl.copy = Copy(None, dst='/bin/helper')
        entry = self.acl.fs_config(binary=False)
        expected_entry = FS_CONFIG_FMT.format(
            'bin/helper', self.USER, self.GROUP, self.PERMS, self.FCAPS)
        self.assertEqual(entry, expected_entry)

    def test_fs_config_binary(self):
        FS_CONFIG_PACK = 'HHHHQ'
        self.acl.copy = Copy(None, dst='/bin/helper')
        entry = self.acl.fs_config(binary=True)
        header = struct.unpack(FS_CONFIG_PACK, entry[:16])
        path = entry[16:]
        self.assertEqual(16+len(path), header[0])
        self.assertEqual(int(self.PERMS, 8), header[1])
        self.assertEqual(int(self.USER), header[2])
        self.assertEqual(int(self.GROUP), header[3])
        self.assertEqual(int(self.FCAPS), header[4])
        self.assertEqual(path, 'bin/helper\x00')

    def test_override_build_use(self):
        self.acl.copy = Copy(None, dst='/bin/helper')
        # Default is True.
        self.assertNotEqual(self.acl.fs_config(), '')
        self.assertNotEqual(self.acl.file_context(), '')
        self.acl.override_build = True
        self.assertNotEqual(self.acl.fs_config(), '')
        self.assertNotEqual(self.acl.file_context(), '')
        self.acl.override_build = False
        self.assertEqual(self.acl.fs_config(), '')
        self.assertEqual(self.acl.file_context(), '')
