#!/bin/bash

# Copyright (C) 2015 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

. tests/test_helpers

setup_working_path

stub_git
stub_tar
stub_curl

echo "Testing that bdk bsp update doesn't override existing files."

# Start with a tree where the user has existing work at a destination.
${BDK} config set os_root "${TEST_TREE}"
mkdir -p ${TEST_TREE}/bsp/tar1

common_args=("-m" "${DATA_DIR}/test_manifest.json")

${BDK} bsp status test_board "${common_args[@]}" |\
 grep "Test Board 0.0.0 - Not Installed"
${BDK} bsp update test_board --accept_licenses "${common_args[@]}"
# Install should complete, but still consider the status "Unrecognized".
${BDK} bsp status test_board "${common_args[@]}" |\
grep "Test Board 0.0.0 - Unrecognized"
