#!/bin/bash

# Copyright (C) 2015 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

. tests/test_helpers
setup_working_path

make_tool() {
  mkdir -p "$1"
  cat <<-EOF > "$1/$2"
	#!/bin/bash
	echo Found me @ $1!
	return 32
	EOF
  chmod a+x "$1/$2"
}

echo "Ensure that aliases installed by envsetup can be used"
echo "even when a users changes product directories."
$BDK product create one board_one
$BDK product create two board_two
cd one
. envsetup.sh
bdk product tool env | grep "Tool path" | grep out-board_one
cd ../two
bdk product tool env | grep "Tool path" | grep out-board_two

tp=$(dirname $(bdk product tool env | grep "Tool path:" | cut -f2- -d: | cut -f2 -d' '))
make_tool "$tp" "adb"
adb | grep "Found me" | grep "$tp"

cd ../one
tp=$(dirname $(bdk product tool env | grep "Tool path:" | cut -f2- -d: | cut -f2 -d' '))
make_tool "$tp" "adb"
adb | grep "Found me" | grep "$tp"

