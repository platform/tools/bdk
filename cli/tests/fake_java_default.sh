#!/bin/bash

# Copyright (C) 2016 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

. tests/test_helpers

setup_working_path

echo "Testing that the first -- is dropped."
$BDK product create product board
cd product
stub_make
. ./envsetup.sh

: 'DISABLED until b/28298438 implemented
mkdir -p config/bdk
echo 'Ensure the default is to use host java.'
rm -f config/bdk/java
bdk product build | grep BRILLO_NO_JAVA=0

echo 'Ensure empty means host java.'
echo > config/bdk/java
bdk product build | grep BRILLO_NO_JAVA=0

echo 'Ensure 0 means fake java.'
echo 0 > config/bdk/java
bdk product build | grep BRILLO_NO_JAVA=1

echo 'Ensure 1 means host java.'
echo 1 > config/bdk/java
bdk product build | grep BRILLO_NO_JAVA=0

echo 'Ensure non-empty+non-one means fake java.'
echo flabbergasted > config/bdk/java
bdk product build | grep BRILLO_NO_JAVA=1
'
