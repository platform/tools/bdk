#!/bin/bash

# Copyright (C) 2015 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

. tests/test_helpers

setup_working_path

echo "Testing default product creation"
$BDK product create product board
echo "Testing valid configuration"

cd product
stub_make
stub_tools

echo "Source envsetup.sh"
. ./envsetup.sh

: 'DISABLED until b/28298438 implemented
echo "Build using m -j20"
m -j20 | grep -- -j20

echo "Build using mm -j12345"
mm -j12345 | grep -q 'ENVCMD=mm'

echo "Testing make dist"
$BDK product build dist | grep -q dist

echo "Testing provision"
provision 1 2 3 4 5 | grep 5
'

echo "Testing adb"
adb 5 4 3 2 1 -help | grep -- -help

echo "Testing fastboot"
fastboot a b c d e f | grep f
