#!/bin/bash

# Copyright (C) 2015 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

. tests/test_helpers

setup_working_path

echo "Make sure that wrapped tool trap codes are returned"
$BDK product create product board
cd product
stub_make
echo 'kill -ABRT $$' >> stubs/make
. ./envsetup.sh
: 'DISABLED until b/28298438 implemented
bdk product build || test $? -eq $((128+6))
'
