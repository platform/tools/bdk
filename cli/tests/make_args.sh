#!/bin/bash

# Copyright (C) 2015 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

. tests/test_helpers

setup_working_path

echo "Testing that the first -- is dropped."
$BDK product create product board
cd product
stub_make
. ./envsetup.sh

: 'DISABLED until b/28298438 implemented
echo 'Make sure the argument immediately after the fixed ones is -j20'
test "$(bdk product build -- -j20 | head -6 | tail -1)" == "-j20"
echo 'Make sure the last argument is --'
test "$(bdk product build -- -j20 -- | head -7 | tail -1)" == "--"
echo 'Make sure the same is true for m'
test "$(m -h | head -6 | tail -1)" == "-h"
test "$(m help | head -6 | tail -1)" == "help"
test "$(m -j20 -- | head -7 | tail -1)" == "--"
echo 'Make sure the same is true for mm'
test "$(mm -h | head -8 | tail -1)" == "-h"
test "$(mm help | head -8 | tail -1)" == "help"
test "$(mm -j20 -- | head -9 | tail -1)" == "--"
'
