#!/bin/bash

# Copyright (C) 2015 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

. tests/test_helpers

setup_working_path

stub_git
stub_tar
stub_curl

common_args=("-m" "${DATA_DIR}/test_manifest.json")

${BDK} config check

echo "Testing bsp status"

# A trivial bsp is "Installed".
${BDK} bsp status void_board "${common_args[@]}" |\
 grep "Void Board 0.0.0 - Installed"

# Start uninstalled.
${BDK} bsp status test_board "${common_args[@]}" |\
 grep "Test Board 0.0.0 - Not Installed"
# Install.
${BDK} bsp install test_board --accept_licenses "${common_args[@]}"
${BDK} bsp status test_board "${common_args[@]}" |\
 grep "Test Board 0.0.0 - Installed"

# Linked.
${BDK} bsp install test_board --accept_licenses --link "${common_args[@]}"
${BDK} bsp status test_board "${common_args[@]}" |\
 grep "Test Board 0.0.0 - Linked"

# Something unexpected in tree.
rm ${TEST_TREE}/bsp/tar1
touch ${TEST_TREE}/bsp/tar1
${BDK} bsp status test_board "${common_args[@]}" |\
 grep "Test Board 0.0.0 - Unrecognized"

# Missing.
#rm ${TODO
