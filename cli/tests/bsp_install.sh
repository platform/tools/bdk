#!/bin/bash

# Copyright (C) 2015 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

. tests/test_helpers

setup_working_path

stub_git
stub_tar
# We don't want curl to happen.
stub_dead_curl

echo "Testing bdk bsp install"
# Note: download, update, and install should all be aliases for the same thing.
# This tests installing from an already present tarball, bsp_download tests
# download starting from nothing in the bdk tree, bsp_update.sh tests updating
# a messed up bdk tree.

${BDK} bsp install test_board -a -m "${DATA_DIR}/test_manifest.json"\
 -e "${DATA_DIR}/package_tree/tarball.tar.gz" | tee output.txt

# All checks are obviously dependent upon how test_board is defined
# in test_manifest.

# Success message expected.
grep "Successfully installed all packages for Test Board." output.txt
# Make sure download happened.
grep "git subpackage license" ${TEST_BSPs}/git_package/source_versions\
/branch\:deadbeef/git_subdir/git_license.txt
tar_source=${TEST_BSPs}/tar_package/source_versions\
/e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855
grep "tar license 1" ${tar_source}/tar/sub1/tar_license1.txt
grep "tar license 2" ${tar_source}/tar/sub2/deeper/tar_license2.txt
